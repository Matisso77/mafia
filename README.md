# Mafia Web Application

## What is it? 
It is a Mafia game in the form of a web application. The server hosts games and the clients connect through their browser

## How to use it?
Currently, one needs to be in a local network with the server, connecting to the localhost of the host machine. This is done for test purposes only, and will be altered (the server will be set up externally)
as the app is further developed.

## Who are we?
We are a group of students from [Wrocław University of Science and Technology](http://ki.pwr.edu.pl/) who believe that one person being excluded from the game in order to be a GM is one person too much. We aim to create a user-friendly way of playing mafia with friends, and never worrying if the number of people isn't too low.

## How does it work?
This piece of software is written using a variety of technologies. The most notable are Java (server), PHP (client), and C# (game engine on server).
There are many interesting things that can be said about the gane, so here's a few solutions we are proud of:
- Our own communication protocol that's both straightforward and frugal in terms of server load
- Huge potential for extensions. The game is essentially made of modules - any new feature/role/faction/scenario takes only a little work to implemenmt
- The game doesn't need your vote! If a player doesn't vote when they should, their vote is omitted. If nobody votes, then a vote will be cast on a random available target.
- Game engine uses multithreading to manage multiple games at once and it frees the memory once a given game ends.


## See any bugs? Tell us
There's bound to be a lot of errors, varying in magnitude. If you stumble upon this piece of software and decide to take a look, please report any bugs you find to our SCRUM master @Matisso77


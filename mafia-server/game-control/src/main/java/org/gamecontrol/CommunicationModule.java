package org.gamecontrol;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class CommunicationModule {
    private InputStream is;
    private OutputStream os;
    public CommunicationModule() throws IOException {
        ServerSocket serverSocket = new ServerSocket(4343, 10);
        Socket socket = serverSocket.accept();
        this.is = socket.getInputStream();
        this.os = socket.getOutputStream();

    }

    public String receiveMsg() {
        byte[] lenBytes = new byte[4];
        try {
            this.is.read(lenBytes, 0, 4);
            int len = (((lenBytes[3] & 0xff) << 24) | ((lenBytes[2] & 0xff) << 16) |
                    ((lenBytes[1] & 0xff) << 8) | (lenBytes[0] & 0xff));
            byte[] receivedBytes = new byte[len];
            this.is.read(receivedBytes, 0, len);
            String received = new String(receivedBytes, 0, len);

            System.out.println("Server received: " + received);
            return received;
        } catch (java.io.IOException exception){
            exception.printStackTrace();
            return "";
        }
    }

    public void sendMsg(String msg){
        byte[] toSendBytes = msg.getBytes();
        int toSendLen = toSendBytes.length;
        byte[] toSendLenBytes = new byte[4];
        toSendLenBytes[0] = (byte) (toSendLen & 0xff);
        toSendLenBytes[1] = (byte) ((toSendLen >> 8) & 0xff);
        toSendLenBytes[2] = (byte) ((toSendLen >> 16) & 0xff);
        toSendLenBytes[3] = (byte) ((toSendLen >> 24) & 0xff);
        try {
            this.os.write(toSendLenBytes);
            this.os.write(toSendBytes);
        } catch (java.io.IOException ignored){
            ignored.printStackTrace();
        }
    }
}

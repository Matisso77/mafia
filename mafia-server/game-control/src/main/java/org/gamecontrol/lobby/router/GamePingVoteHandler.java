package org.gamecontrol.lobby.router;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.core.Exceptions.LobbyNotFoundException;
import org.gamecontrol.lobby.business.Lobby;
import org.gamecontrol.lobby.business.LobbyManager;

import java.util.HashMap;
import java.util.Map;

class GamePingVoteHandler {

    private LobbyManager lobbyManager;
    private final static int OK_CODE = 200;

    GamePingVoteHandler(LobbyManager lobbyManager) {
        this.lobbyManager = lobbyManager;
    }

    boolean isDead(String player, int number) {
        Map<String, Boolean> playersState = new HashMap<>();
        try {
            playersState = lobbyManager.getLobby(number).playerAliveList;
        } catch (LobbyNotFoundException e) {
            e.printStackTrace();
        }
        for (Map.Entry<String, Boolean> entry : playersState.entrySet()) {
            String key = entry.getKey();
            Boolean value = entry.getValue();
            if (key.equals(player)) {
                return !value;
            }
        }
        return false;
    }

    void gameVote(RoutingContext routingContext) {
        Integer number = routingContext.getBodyAsJson().getInteger("roomNumber");
        String player = routingContext.getBodyAsJson().getString("player");
        JsonObject list = null;


        try {
            Lobby lobby = lobbyManager.getLobby(number);

            if (lobby.endOfgame) {
                list = new JsonObject().put("message", lobby.whoWon);
            } else if (isDead(player, number)) {
                //list = new JsonObject().put("message", lobby.whoWon);
            } else if (lobby.isDay()) {
                if (lobby.endOfVotingDay) {
                    list = new JsonObject()
                            .put("WhoKilledDay", lobby.whoHasBeenKilledNight)
                            .put("DayNumber", lobby.getDayNumber());
                } else if (lobby.getIfRecivedMessage(player)) {
                    lobby.setWhoRecivedMessage(player);
                    list = new JsonObject()
                            .put("dayNumber", lobby.getDayNumber())
                            .put("targetList", lobby.getTargerPlayer());

                    if (lobby.playerIsPoliecman(player) && lobby.getDayNumber() > 2) {
                        list.put("policeman", lobby.getPolicmanOpinion());
                        list.put("playerPoliceman", lobby.tributeIfVariableToMatisso);
                    }
                } else {
                    list = new JsonObject().put("information", "You already got information");
                }
            } else if (!lobby.isDay() && lobby.getDayNumber() > 1) {
                list = new JsonObject();
                if (lobby.playerIsPoliecman(player)) {
                    if (lobby.getPoliceFlag()) {
                        list.put("listOfTargetsP", lobby.listOfTargetP)
                                .put("Night", lobby.getDayNumber());
                        lobby.setPoliceFlag();
                    }
                } else if (lobby.playerIsMafia(player)) {
                    if (lobby.getMafiaFlag(player)) {
                        list.put("listOfTargetsM", lobby.listOfTargetM)
                                .put("Night", lobby.getDayNumber());
                        lobby.whoVotedNight.replace(player, false);
                    }
                } else {
                    list.put("Night", lobby.getDayNumber());
                }
            } else if (!lobby.isDay() && lobby.getDayNumber() == 1) {
                list = new JsonObject();
                list.put("Night", "1");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObject responseJson = new JsonObject().put("list", list);
        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(OK_CODE)
                .end(responseJson.toString());
    }
}

package org.gamecontrol.lobby.router;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.core.Exceptions.LobbyNotFoundException;
import org.gamecontrol.lobby.business.LobbyManager;
import org.gamecontrol.lobby.exceptionHandler.LobbyExceptionHandler;

class SendRoles {

    private LobbyManager lobbyManager;
    private LobbyExceptionHandler lobbyExceptionHandler;

    SendRoles(LobbyManager lobbyManager, LobbyExceptionHandler lobbyExceptionHandler) {
        this.lobbyManager = lobbyManager;
        this.lobbyExceptionHandler = lobbyExceptionHandler;
    }

    void sendRoles(RoutingContext routingContext) {
        try {
            trySend(routingContext);
        } catch (NullPointerException e) {
            lobbyExceptionHandler.badRequest(routingContext);
        }
    }

    private void trySend(RoutingContext routingContext) throws NullPointerException {
        JsonObject responseJson = prepareResponse(routingContext);
        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(200)
                .end(responseJson.toString());
    }

    private JsonObject prepareResponse(RoutingContext routingContext) throws NullPointerException {
        Integer roomNumber = routingContext.getBodyAsJson().getInteger("roomNumber");
        String player = routingContext.getBodyAsJson().getString("player");
        String roles = "";
        try {
            roles = lobbyManager
                    .getRoles(roomNumber)
                    .get(player);
        } catch (LobbyNotFoundException e) {
            e.printStackTrace();
        }
        return new JsonObject().put("role", roles);
    }
}

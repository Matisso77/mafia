package org.gamecontrol.lobby.router;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.lobby.business.LobbyManager;
import org.gamecontrol.lobby.exceptionHandler.LobbyExceptionHandler;

class SendGameData {

    private LobbyManager lobbyManager;
    private LobbyExceptionHandler lobbyExceptionHandler;

    SendGameData(LobbyManager lobbyManager, LobbyExceptionHandler lobbyExceptionHandler) {
        this.lobbyManager = lobbyManager;
        this.lobbyExceptionHandler = lobbyExceptionHandler;
    }

    void sendData(RoutingContext routingContext) {
        try {
            validData(routingContext);
            JsonObject responseJson = new JsonObject().put("Message", "Data delivered");
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(200)
                    .end(responseJson.toString());
        } catch (NullPointerException e) {
            lobbyExceptionHandler.badRequest(routingContext);
        }
    }

    private void validData(RoutingContext routingContext) throws NullPointerException {
        Integer roomNumber = routingContext.getBodyAsJson().getInteger("roomNumber");
        String vote = routingContext.getBodyAsJson().getString("vote");
        String whoVote = routingContext.getBodyAsJson().getString("whoVote");
        String whoGetVote = routingContext.getBodyAsJson().getString("whoGetVote");
        if (roomNumber == null || vote == null || whoVote == null || whoGetVote == null)
            throw new NullPointerException();
        lobbyManager.sendVote(roomNumber, vote, whoVote, whoGetVote);
    }
}

package org.gamecontrol.lobby.exceptionHandler;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class PlayerNumberStartHandler {

    private int ERR_CODE = 613;

    void handle(RoutingContext routingContext) {
        JsonObject responseJson = new JsonObject().put("Message", "Room is not full!");
        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(ERR_CODE)
                .end(responseJson.toString());
    }
}
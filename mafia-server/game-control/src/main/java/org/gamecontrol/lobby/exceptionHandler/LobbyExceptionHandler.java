package org.gamecontrol.lobby.exceptionHandler;

import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.core.Exceptions.router.BadRequestHandler;

public class LobbyExceptionHandler {

    private BadRequestHandler leaveRoomHandler = new BadRequestHandler();
    private RoomNotFoundHandler roomNotFoundHandler = new RoomNotFoundHandler();
    private WrongPasswordHandler wrongPasswordHandler = new WrongPasswordHandler();
    private FullRoomHandler fullRoomHandler = new FullRoomHandler();
    private PlayerNumberStartHandler playerNumberStartHandler = new PlayerNumberStartHandler();

    public void badRequest(RoutingContext routingContext) {
        leaveRoomHandler.handle(routingContext);
    }

    public void roomNotFound(RoutingContext routingContext) {
        roomNotFoundHandler.handle(routingContext);
    }

    public void wrongPassword(RoutingContext routingContext) {
        wrongPasswordHandler.handle(routingContext);
    }

    public void fullRoom(RoutingContext routingContext) {
        fullRoomHandler.handle(routingContext);
    }

    public void playerStart(RoutingContext routingContext) {
        playerNumberStartHandler.handle(routingContext);
    }
}

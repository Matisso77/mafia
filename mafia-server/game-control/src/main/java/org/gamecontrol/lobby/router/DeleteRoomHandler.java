package org.gamecontrol.lobby.router;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.lobby.business.LobbyManager;
import org.gamecontrol.lobby.exceptionHandler.LobbyExceptionHandler;

class DeleteRoomHandler {

    private final LobbyManager lobbyManager;
    private final LobbyExceptionHandler lobbyExceptionHandler;
    private final static int OK_CODE = 200;

    DeleteRoomHandler(LobbyManager lobbyManager, LobbyExceptionHandler lobbyExceptionHandler) {
        this.lobbyManager = lobbyManager;
        this.lobbyExceptionHandler = lobbyExceptionHandler;
    }

    void deleteRoom(RoutingContext routingContext) {
        if (requestForDeleteRoom(routingContext)) {
            JsonObject response = new JsonObject().put("Message","Room Deleted!");
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(OK_CODE)
                    .end(response.toString());
        } else {
            lobbyExceptionHandler.roomNotFound(routingContext);
        }
    }

    private boolean requestForDeleteRoom(RoutingContext routingContext) {
        Integer roomNumber = routingContext.getBodyAsJson().getInteger("roomNumber");
        if (roomNumber == null)
            return false;
        return lobbyManager.deleteRoom(roomNumber);
    }
}

package org.gamecontrol.lobby.router;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.lobby.business.LobbyManager;
import org.gamecontrol.lobby.exceptionHandler.LobbyExceptionHandler;

class CreateRoomHandler {

    private final LobbyManager lobbyManager;
    private final LobbyExceptionHandler lobbyExceptionHandler;
    private final static int OK_CODE = 200;

    CreateRoomHandler(LobbyManager lobbyManager, LobbyExceptionHandler lobbyExceptionHandler) {
        this.lobbyManager = lobbyManager;
        this.lobbyExceptionHandler = lobbyExceptionHandler;
    }

    void createRoom(RoutingContext routingContext) {
        int roomNumber = prepareRoom(routingContext);
        if (roomNumber >= 0) {
            JsonObject response = new JsonObject().put("Message", "Room Created!").put("roomNumber", roomNumber);
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(OK_CODE)
                    .end(response.toString());
        } else {
            lobbyExceptionHandler.badRequest(routingContext);
        }
    }

    private int prepareRoom(RoutingContext routingContext) {
        String password = routingContext.getBodyAsJson().getString("roomPassword");
        Integer size = routingContext.getBodyAsJson().getInteger("roomSize");
        String username = routingContext.getBodyAsJson().getString("username");
        if (password == null || size == null || username == null)
            return -1;
        return lobbyManager.createRoom(password, size, username);
    }
}

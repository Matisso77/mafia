package org.gamecontrol.lobby.router;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.core.Exceptions.LobbyNotFoundException;
import org.gamecontrol.lobby.business.LobbyManager;
import org.gamecontrol.lobby.exceptionHandler.LobbyExceptionHandler;

import java.util.HashMap;
import java.util.Map;

public class GamePingHandler {


    private LobbyManager lobbyManager;
    private final static int OK_CODE = 200;

    GamePingHandler(LobbyManager lobbyManager) {
        this.lobbyManager = lobbyManager;
    }

    void getGameData(RoutingContext routingContext) {
        Integer number = routingContext.getBodyAsJson().getInteger("roomNumber");
        String player = routingContext.getBodyAsJson().getString("player");

        Map<String, Boolean> playersState = new HashMap<>();
        try {
            playersState = lobbyManager.getLobby(number).playerAliveList;
        } catch (LobbyNotFoundException e) {
            e.printStackTrace();
        }
        JsonObject list = new JsonObject();

        for (Map.Entry<String, Boolean> entry : playersState.entrySet()) {
            String key = entry.getKey();
            Boolean value = entry.getValue();
            if (!value) {
                list.put(key, "DEAD");
            } else {
                if (key.equals(player)) {
                    list.put(key, "MYSELF");
                } else {
                    list.put(key, "ALIVE");
                }
            }
        }

        JsonObject responseJson = new JsonObject().put("list", list);

        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(OK_CODE)
                .end(responseJson.toString());
    }
}

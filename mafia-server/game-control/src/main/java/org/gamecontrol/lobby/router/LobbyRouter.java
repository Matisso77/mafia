package org.gamecontrol.lobby.router;

import io.vertx.ext.web.Router;
import org.gamecontrol.lobby.business.LobbyManager;
import org.gamecontrol.lobby.exceptionHandler.LobbyExceptionHandler;


public class LobbyRouter {

    private GamePingHandler gamePingHandler;
    private LeaveRoomHandler leaveRoomHandler;

    private final LobbyExceptionHandler lobbyExceptionHandler;
    private GetPlayerList getPlayerList;
    private CreateRoomHandler createRoomHandler;
    private JoinRoomHandler joinRoomHandler;
    private ExistingRoomHandler existingRoomHandler;
    private DeleteRoomHandler deleteRoomHandler;
    private StartGameHandler startGameHandler;
    private CheckStatusHandler checkStatusHandler;
    private GetGameData getGameData;
    private SendGameData sendData;
    private GamePingVoteHandler gamePingVoteHandler;
    private SendRoles sendRoles;

    public LobbyRouter(LobbyManager lobbyManager) {
        lobbyExceptionHandler = new LobbyExceptionHandler();
        existingRoomHandler = new ExistingRoomHandler(lobbyManager);
        deleteRoomHandler = new DeleteRoomHandler(lobbyManager, lobbyExceptionHandler);
        createRoomHandler = new CreateRoomHandler(lobbyManager, lobbyExceptionHandler);
        joinRoomHandler = new JoinRoomHandler(lobbyManager, lobbyExceptionHandler);
        getPlayerList = new GetPlayerList(lobbyManager, lobbyExceptionHandler);
        leaveRoomHandler = new LeaveRoomHandler(lobbyManager, lobbyExceptionHandler);
        startGameHandler = new StartGameHandler(lobbyManager, lobbyExceptionHandler);
        checkStatusHandler = new CheckStatusHandler(lobbyManager, lobbyExceptionHandler);
        getGameData = new GetGameData(lobbyManager, lobbyExceptionHandler);
        sendData = new SendGameData(lobbyManager, lobbyExceptionHandler);
        gamePingHandler = new GamePingHandler(lobbyManager);
        gamePingVoteHandler = new GamePingVoteHandler(lobbyManager);
        sendRoles = new SendRoles(lobbyManager, lobbyExceptionHandler);
    }

    public Router setLobbyApiRouting(Router router) {
        router.post("/api/lobby/create").handler(createRoomHandler::createRoom);
        router.post("/api/lobby/join").handler(joinRoomHandler::joinRoom);
        router.post("/api/lobby/exist").handler(existingRoomHandler::roomExists);
        router.post("/api/lobby/players").handler(getPlayerList::getPlayers);
        router.post("/api/lobby/delete").handler(deleteRoomHandler::deleteRoom);
        router.post("/api/lobby/leave").handler(leaveRoomHandler::leave);
        router.post("/api/lobby/start").handler(startGameHandler::start);
        router.post("/api/lobby/status").handler(checkStatusHandler::check);
        router.post("/api/lobby/gameData").handler(getGameData::getGameData);
        router.post("/api/lobby/sendData").handler(sendData::sendData);
        router.post("/api/lobby/gamePing").handler(gamePingHandler::getGameData);
        router.post("/api/lobby/gamePingVote").handler(gamePingVoteHandler::gameVote);
        router.post("/api/lobby/sendRoles").handler(sendRoles::sendRoles);
        return router;
    }
}

package org.gamecontrol.lobby.router;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.core.Exceptions.LobbyNotFoundException;
import org.gamecontrol.lobby.business.LobbyManager;
import org.gamecontrol.lobby.exceptionHandler.LobbyExceptionHandler;

class LeaveRoomHandler {
    private final static int OK_CODE = 200;
    private final LobbyManager lobbyManager;
    private final LobbyExceptionHandler lobbyExceptionHandler;

    LeaveRoomHandler(LobbyManager lobbyManager, LobbyExceptionHandler lobbyExceptionHandler) {
        this.lobbyManager = lobbyManager;
        this.lobbyExceptionHandler = lobbyExceptionHandler;
    }

    void leave(RoutingContext routingContext) {
        try {
            tryLeave(routingContext);
        } catch (LobbyNotFoundException e) {
            lobbyExceptionHandler.roomNotFound(routingContext);
        } catch (NullPointerException e) {
            lobbyExceptionHandler.badRequest(routingContext);
        }
    }

    private void tryLeave(RoutingContext routingContext) throws LobbyNotFoundException, NullPointerException {
        requestForLeave(routingContext);
        JsonObject response = new JsonObject().put("Message", "You leave room!");
        routingContext.response()
                .putHeader("Content-Type", "application/json")
                .setStatusCode(OK_CODE)
                .end(response.toString());
    }

    private void requestForLeave(RoutingContext routingContext) throws NullPointerException, LobbyNotFoundException {
        Integer roomNumber = routingContext.getBodyAsJson().getInteger("roomNumber");
        String userName = routingContext.getBodyAsJson().getString("userName");
        if (roomNumber == null || userName == null)
            throw new NullPointerException();
        lobbyManager.leaveRoom(roomNumber, userName);
    }
}

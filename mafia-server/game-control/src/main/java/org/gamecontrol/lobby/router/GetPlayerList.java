package org.gamecontrol.lobby.router;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.core.Exceptions.LobbyNotFoundException;
import org.gamecontrol.lobby.business.LobbyManager;
import org.gamecontrol.lobby.exceptionHandler.LobbyExceptionHandler;

class GetPlayerList {

    private LobbyManager lobbyManager;
    private final LobbyExceptionHandler lobbyExceptionHandler;
    private final static int OK_CODE = 200;

    GetPlayerList(LobbyManager lobbyManager, LobbyExceptionHandler lobbyExceptionHandler) {
        this.lobbyManager = lobbyManager;
        this.lobbyExceptionHandler = lobbyExceptionHandler;
    }

    void getPlayers(RoutingContext routingContext) {
        try {
            tryGetPlayer(routingContext);
        } catch (LobbyNotFoundException e) {
            lobbyExceptionHandler.roomNotFound(routingContext);
        } catch (NullPointerException ex) {
            lobbyExceptionHandler.badRequest(routingContext);
        }
    }

    private void tryGetPlayer(RoutingContext routingContext) throws LobbyNotFoundException, NullPointerException {
        Integer number = routingContext.getBodyAsJson().getInteger("roomNumber");
        JsonObject responseJson = new JsonObject().put("list", lobbyManager.getPlayerList(number))
                .put("status", lobbyManager.getGameStatus(number));
        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(OK_CODE)
                .end(responseJson.toString());
    }
}

package org.gamecontrol.lobby.business;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.gamecontrol.CommunicationModule;
import org.gamecontrol.core.Exceptions.FullLobbyException;
import org.gamecontrol.core.Exceptions.LobbyNotFoundException;
import org.gamecontrol.core.Exceptions.LobbyPasswordException;
import org.gamecontrol.core.Exceptions.PlayerNumberStartException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class LobbyManager {

    private static Logger LOGGER = LoggerFactory.getLogger(LobbyManager.class);
    private HashMap<Integer, Lobby> lobbies = new HashMap<>();
    private CommunicationModule communicationModule ;

    public LobbyManager() throws IOException {

        this.communicationModule = new CommunicationModule();
        RunnableDemo R1 = new RunnableDemo("Thread-1", communicationModule);
        R1.start();
    }



    class RunnableDemo implements Runnable {
        CommunicationModule cm;
        String command;
        private Thread t;
        private String threadName;
        private String[] commandList;

        RunnableDemo(String name, CommunicationModule rmm) {
            this.cm = rmm;
            threadName = name;
            System.out.println("Creating " + threadName);
        }

        public void run() {
            System.out.println("Running " + threadName);

            while (true) {
                command = cm.receiveMsg();
                System.out.println(command);
                commandList=command.split(";");
                System.out.println(commandList[0]);
                try {
                    getLobby(Integer.parseInt(commandList[0])).handleMessage(commandList);
                } catch (LobbyNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        void start() {
            System.out.println("Starting " + threadName);
            if (t == null) {
                t = new Thread(this, threadName);
                t.start();
            }
        }
    }
    /**
     * Creating room in lobby
     *
     * @param password Room Password
     * @param size     Room Size
     * @param username User Name
     * @return integer with room name as a positive number, if error then return -1.
     */
    public int createRoom(String password, int size, String username) {

        Lobby lobby = new Lobby(size, password, username);
        lobbies.put(lobby.getId(), lobby);
        String message = "Creating room with id: " + lobby.getId() +
                "\n Password: " + password +
                "\n Room size: " + size;
        LOGGER.info(message);
        return lobby.getId();
    }


    /**
     * Joining player to room
     *
     * @param userName     User Name
     * @param roomNumber   Room Number
     * @param roomPassword Room Password
     * @return room Size, if room not exists return -1 else if room is full return -2
     */
    public int joinRoom(String userName, int roomNumber, String roomPassword) throws FullLobbyException, LobbyPasswordException, LobbyNotFoundException {
        String message = "Join player: " + userName +
                "\n Room Number: " + roomNumber +
                "\n Room password: " + roomPassword;
        LOGGER.info(message);
        return getLobby(roomNumber).addPlayer(userName, roomPassword);
    }


    /**
     * @param userName   User name
     * @param roomNumber Room number
     * @return if room with specific number exists and player with specific name are in room return true
     * else return false.
     */
    public boolean roomExists(String userName, int roomNumber) throws LobbyNotFoundException {
        String message = "Existence room check: " + userName +
                "\n RoomNumber: " + roomNumber;
        LOGGER.info(message);
        return lobbies.containsKey(roomNumber) && getLobby(roomNumber).getPlayerList().contains(userName);
    }


    /**
     * @param roomNumber roomNumber
     * @return true if room was deleted else false;
     */
    public boolean deleteRoom(int roomNumber) {
        String message = "Deleted room with number: " + roomNumber;
        LOGGER.info(message);
        lobbies.remove(roomNumber);
        return true;
    }

    public List<String> getPlayerList(int roomNumber) throws LobbyNotFoundException {
        return getLobby(roomNumber).getPlayerList();
    }
    public Map<String, String> getRoles(int roomNumber)throws LobbyNotFoundException{
        return getLobby(roomNumber).getRoles();
    }
    public String getGameStatus(int roomNumber) throws LobbyNotFoundException {
        return String.valueOf(getLobby(roomNumber).getGameStatus());

    }
    public boolean leaveRoom(Integer roomNumber, String userName) throws LobbyNotFoundException {
        Lobby lobby = getLobby(roomNumber);
        if (lobby.getPlayerList().size() == 1)
            deleteRoom(roomNumber);
        lobby.removePlayer(userName);
        return true;
    }

    public Lobby getLobby(int roomNumber) throws LobbyNotFoundException {
        Lobby lobby = lobbies.get(roomNumber);
        if (Objects.isNull(lobby))
            throw new LobbyNotFoundException();
        return lobby;
    }

    public boolean startGame(int roomNumber) throws LobbyNotFoundException, PlayerNumberStartException {
        Lobby lobby = getLobby(roomNumber);
        System.out.println(roomNumber);
        if (!lobby.ifFull())
            throw new PlayerNumberStartException();
        setStart(roomNumber);
        StringBuilder playersMsg = new StringBuilder();
        int players = 0;
        for (String player : getPlayerList(roomNumber)){
            playersMsg.append(player + ":");
            players += 1;
        }
        playersMsg.deleteCharAt(playersMsg.length()-1);
        communicationModule.sendMsg(roomNumber+";"+"C;"+playersMsg+";"+players);
        return true;
    }

    public boolean sendVote(Integer roomNumber, String vote, String whoVote, String whoGetVote) {
        communicationModule.sendMsg(roomNumber+";"+ vote+";"+ whoVote+";"+whoGetVote);
        return true;
    }
    public boolean gameStatus(int roomNumber) throws LobbyNotFoundException {
        return getLobby(roomNumber).getGameStatus();
    }

    public void setStart(int roomNumber) {
        try {
            getLobby(roomNumber).start();

        } catch (LobbyNotFoundException e) {
            e.printStackTrace();
        }
    }
}

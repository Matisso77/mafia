package org.gamecontrol.lobby.router;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.core.Exceptions.LobbyNotFoundException;
import org.gamecontrol.core.Exceptions.PlayerNumberStartException;
import org.gamecontrol.lobby.business.LobbyManager;
import org.gamecontrol.lobby.exceptionHandler.LobbyExceptionHandler;

class StartGameHandler {

    private LobbyManager lobbyManager;
    private LobbyExceptionHandler lobbyExceptionHandler;

    StartGameHandler(LobbyManager lobbyManager, LobbyExceptionHandler lobbyExceptionHandler) {
        this.lobbyManager = lobbyManager;
        this.lobbyExceptionHandler = lobbyExceptionHandler;
    }

    void start(RoutingContext routingContext) {
        try {
            tryStartGame(routingContext);
        } catch (LobbyNotFoundException e) {
            lobbyExceptionHandler.roomNotFound(routingContext);
        } catch (PlayerNumberStartException e) {
            lobbyExceptionHandler.playerStart(routingContext);
        } catch (NullPointerException e) {
            lobbyExceptionHandler.badRequest(routingContext);
        }
    }

    private void tryStartGame(RoutingContext routingContext) throws LobbyNotFoundException, PlayerNumberStartException, NullPointerException {
        validData(routingContext);
        JsonObject responseJson = new JsonObject().put("Message", "Start requested!");
        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(200)
                .end(responseJson.toString());
    }

    private boolean validData(RoutingContext routingContext) throws LobbyNotFoundException, PlayerNumberStartException, NullPointerException {
        Integer roomNumber = routingContext.getBodyAsJson().getInteger("roomNumber");
        System.out.println(roomNumber);
        if (roomNumber == null)
            throw new NullPointerException();
        return lobbyManager.startGame(roomNumber);
    }
}

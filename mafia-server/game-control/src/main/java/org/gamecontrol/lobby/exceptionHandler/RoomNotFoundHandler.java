package org.gamecontrol.lobby.exceptionHandler;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

class RoomNotFoundHandler {

    private final static int ERR_CODE = 610;

    void handle(RoutingContext routingContext) {
        JsonObject response = new JsonObject().put("Message", "Room not found!");
        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(ERR_CODE)
                .end(response.toString());
    }
}

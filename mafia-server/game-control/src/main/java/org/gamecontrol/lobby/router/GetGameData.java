package org.gamecontrol.lobby.router;

import com.google.gson.Gson;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.core.Exceptions.LobbyNotFoundException;
import org.gamecontrol.lobby.business.LobbyManager;
import org.gamecontrol.lobby.exceptionHandler.LobbyExceptionHandler;

class GetGameData {


    private LobbyManager lobbyManager;
    private final LobbyExceptionHandler lobbyExceptionHandler;
    private final static int OK_CODE = 200;

    GetGameData(LobbyManager lobbyManager, LobbyExceptionHandler lobbyExceptionHandler) {
        this.lobbyManager = lobbyManager;
        this.lobbyExceptionHandler = lobbyExceptionHandler;
    }

    void getGameData(RoutingContext routingContext) {
        try {
            Integer number = routingContext.getBodyAsJson().getInteger("roomNumber");
            String response = new Gson().toJson(lobbyManager.getPlayerList(number));
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(OK_CODE)
                    .end(response);
        } catch (LobbyNotFoundException e) {
            lobbyExceptionHandler.roomNotFound(routingContext);
        } catch (NullPointerException ex) {
            lobbyExceptionHandler.badRequest(routingContext);
        }

    }
}
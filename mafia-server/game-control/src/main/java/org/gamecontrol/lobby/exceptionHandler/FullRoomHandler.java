package org.gamecontrol.lobby.exceptionHandler;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

class FullRoomHandler {
    private final static int ERR_CODE = 611;

    void handle(RoutingContext routingContext) {
        JsonObject responseJson = new JsonObject().put("Message", "Room is full!");
        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(ERR_CODE)
                .end(responseJson.toString());
    }
}

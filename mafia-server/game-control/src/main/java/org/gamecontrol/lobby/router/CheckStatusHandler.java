package org.gamecontrol.lobby.router;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.core.Exceptions.LobbyNotFoundException;
import org.gamecontrol.lobby.business.LobbyManager;
import org.gamecontrol.lobby.exceptionHandler.LobbyExceptionHandler;

class CheckStatusHandler {

    private final static int NOT_START_CODE = 614;
    private LobbyManager lobbyManager;
    private LobbyExceptionHandler lobbyExceptionHandler;

    CheckStatusHandler(LobbyManager lobbyManager, LobbyExceptionHandler lobbyExceptionHandler) {
        this.lobbyManager = lobbyManager;
        this.lobbyExceptionHandler = lobbyExceptionHandler;
    }

    void check(RoutingContext routingContext) {
        try {
            boolean status = isStarted(routingContext);
            JsonObject responseJson = new JsonObject().put("Status", status);
            int statusCode = status ? 200 : NOT_START_CODE;
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(statusCode)
                    .end(responseJson.toString());
        } catch (LobbyNotFoundException e) {
            lobbyExceptionHandler.roomNotFound(routingContext);
        } catch (NullPointerException e) {
            lobbyExceptionHandler.badRequest(routingContext);
        }
    }

    private boolean isStarted(RoutingContext routingContext) throws LobbyNotFoundException, NullPointerException {
        Integer roomNumber = routingContext.getBodyAsJson().getInteger("roomNumber");
        if (roomNumber == null)
            throw new NullPointerException();
        return lobbyManager.gameStatus(roomNumber);
    }
}

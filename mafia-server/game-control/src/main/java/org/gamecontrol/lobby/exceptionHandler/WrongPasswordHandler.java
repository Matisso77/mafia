package org.gamecontrol.lobby.exceptionHandler;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

class WrongPasswordHandler {

    private final static int ERR_CODE = 612;

    void handle(RoutingContext routingContext) {
        JsonObject responseJson = new JsonObject().put("Message", "Wrong Password!");
        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(ERR_CODE)
                .end(responseJson.toString());
    }
}

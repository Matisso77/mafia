package org.gamecontrol.lobby.business;

import org.gamecontrol.core.Exceptions.FullLobbyException;
import org.gamecontrol.core.Exceptions.LobbyPasswordException;

import java.util.*;

public class Lobby {

    private final static Random random = new Random();
    private final int id;
    private final int size;
    private final String password;
    private List<String> playerList = new LinkedList<>();
    private boolean isStarted = false;
    private Map<String, Boolean> playersStatus;
    public Map<String, Boolean> playerAliveList;
    private boolean policmanHasRight = false;
    public String whoHasBeenKilledNight = "false";
    private Map<String, String> playerRole;
    private boolean isDay = false;
    private int dayNumber = 1;
    public boolean endOfVotingDay = false;
    private Boolean policemanFlag = false;
    private String listOfTarget;
    public String listOfTargetM;
    public String listOfTargetP;
    public Map<String, Boolean> whoVotedNight;
    public boolean endOfgame = false;
    public String whoWon = "";
    public String tributeIfVariableToMatisso = "";

    Lobby(int size, String password, String userName) {
        id = random.nextInt(10000);
        this.size = size;
        this.password = password;
        playerList.add(userName);
        playerRole = new HashMap<>();
        playersStatus = new HashMap<>();
        whoVotedNight = new HashMap<>();
        playerAliveList = new HashMap<>();
    }

    public boolean getMafiaFlag(String player) {
        return whoVotedNight.get(player);
    }

    public boolean getPoliceFlag() {
        return policemanFlag;
    }

    public void setPoliceFlag() {
        policemanFlag = false;
    }

    int addPlayer(String player, String password) throws FullLobbyException, LobbyPasswordException {
        if (ifFull())
            throw new FullLobbyException();
        if (!this.password.equals(password))
            throw new LobbyPasswordException();
        if (!playerList.contains(player))
            playerList.add(player);
        return size;
    }

    void removePlayer(String player) {
        playerList.remove(player);
    }

    List<String> getPlayerList() {
        return playerList;
    }

    int getId() {
        return id;
    }

    public boolean isDay() {
        return this.isDay;
    }

    public boolean getPolicmanOpinion() {
        return this.policmanHasRight;
    }

    boolean ifFull() {
        return size == playerList.size();
    }

    String getPassword() {
        return password;
    }

    public void start() {
        isStarted = true;
        playerList.forEach((key) -> playersStatus.put(key, true));
        listOfTarget = playerList.toString();
        playerList.forEach((key) -> whoVotedNight.put(key, false));
        playerList.forEach((key) -> playerAliveList.put(key, true));

    }

    public boolean getGameStatus() {
        return isStarted;
    }

    public void setWhoRecivedMessage(String whoRecived) {
        playersStatus.replace(whoRecived, false);
    }

    public boolean getIfRecivedMessage(String whoRecived) {
        return playersStatus.get(whoRecived);
    }

    public void handleMessage(String[] commandList) {

        String[] tmp;
        String[] tmp2;
        switch (commandList[1]) {
            case "S":
                if (commandList[2].equals("DAY")) {

                    isDay = true;

                    endOfVotingDay = false;

                    playerList.forEach((key) -> playersStatus.put(key, true));


                } else {
                    isDay = false;
                }
                dayNumber = Integer.parseInt(commandList[3]);
                break;
            case "P":
                tmp = commandList[2].split(":");
                for (String s : tmp) {
                    tmp2 = s.split(",");
                    playerRole.put(tmp2[0], tmp2[1]);
                }
                break;
            case "R":

                whoHasBeenKilledNight = commandList[2];
                endOfVotingDay = true;

                playerAliveList.replace(commandList[2], false);


                break;
            case "K":
                playerAliveList.replace(commandList[2], false);
                break;
            case "W":
                endOfgame = true;
                whoWon = (commandList[2]);

                break;
            case "V":
                listOfTarget = commandList[3];
                break;
            case "T":
                policmanHasRight = Boolean.parseBoolean(commandList[3]);
                tributeIfVariableToMatisso = commandList[2];
                break;
            case "N":

                if (commandList[2].equals("101")) {
                    policemanFlag = true;
                    listOfTargetP = commandList[4];
                } else if (commandList[2].equals("200")) {
                    listOfTargetM = commandList[4];
                    policemanFlag = false;
                    playerList.forEach((key) -> whoVotedNight.put(key, true));
                } else {

                    policemanFlag = false;
                }
                break;
        }

    }

    public Map<String, String> getRoles() {
        return playerRole;
    }

    public boolean playerIsPoliecman(String player) {
        return playerRole.get(player).equals("Cop");
    }

    public boolean playerIsMafia(String player) {
        return playerRole.get(player).equals("Mobster");
    }

    public int getDayNumber() {
        return dayNumber;
    }

    public String getTargerPlayer() {
        return listOfTarget;
    }
}

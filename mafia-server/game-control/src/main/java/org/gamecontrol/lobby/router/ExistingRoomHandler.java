package org.gamecontrol.lobby.router;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.core.Exceptions.LobbyNotFoundException;
import org.gamecontrol.lobby.business.LobbyManager;

class ExistingRoomHandler {
    private final LobbyManager lobbyManager;
    private final static int ERR_CODE = 400;

    ExistingRoomHandler(LobbyManager lobbyManager) {
        this.lobbyManager = lobbyManager;
    }

    void roomExists(RoutingContext routingContext) {
        if (prepareRoom(routingContext)) {
            JsonObject response = new JsonObject().put("Message","You are in room");
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(200)
                    .end(response.toString());
        } else {
            JsonObject responseJson = new JsonObject().put("Message", "Room not exists or you are not in this room!");
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(ERR_CODE)
                    .end(responseJson.toString());
        }
    }

    private boolean prepareRoom(RoutingContext routingContext) {
        String userName = routingContext.getBodyAsJson().getString("userName");
        Integer roomNumber = routingContext.getBodyAsJson().getInteger("roomNumber");
        if (userName == null || roomNumber == null)
            return false;
        try {
            return lobbyManager.roomExists(userName,roomNumber);
        } catch (LobbyNotFoundException e) {
            return false;
        }
    }
}

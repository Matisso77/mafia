package org.gamecontrol.lobby.router;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.core.Exceptions.FullLobbyException;
import org.gamecontrol.core.Exceptions.LobbyNotFoundException;
import org.gamecontrol.core.Exceptions.LobbyPasswordException;
import org.gamecontrol.lobby.business.LobbyManager;
import org.gamecontrol.lobby.exceptionHandler.LobbyExceptionHandler;

class JoinRoomHandler {

    private final LobbyManager lobbyManager;
    private final static int OK_CODE = 200;
    private final LobbyExceptionHandler lobbyExceptionHandler;

    JoinRoomHandler(LobbyManager lobbyManager, LobbyExceptionHandler lobbyExceptionHandler) {
        this.lobbyManager = lobbyManager;
        this.lobbyExceptionHandler = lobbyExceptionHandler;
    }

    void joinRoom(RoutingContext routingContext) {
        try {
            tryJoin(routingContext);
        } catch (NullPointerException e) {
            lobbyExceptionHandler.badRequest(routingContext);
        } catch (FullLobbyException e) {
            lobbyExceptionHandler.fullRoom(routingContext);
        } catch (LobbyPasswordException e) {
            lobbyExceptionHandler.wrongPassword(routingContext);
        } catch (LobbyNotFoundException e) {
            lobbyExceptionHandler.roomNotFound(routingContext);
        }
    }

    private void tryJoin(RoutingContext routingContext) throws FullLobbyException, LobbyPasswordException, LobbyNotFoundException, NullPointerException {
        int size = prepareJoin(routingContext);
        Integer roomNumber = routingContext.getBodyAsJson().getInteger("roomNumber");
        JsonObject responseJson = new JsonObject().put("Message", "Joined to room!")
                .put("roomNumber", roomNumber)
                .put("roomSize", size);

        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(OK_CODE)
                .end(responseJson.toString());
    }

    private int prepareJoin(RoutingContext routingContext) throws FullLobbyException, LobbyPasswordException, LobbyNotFoundException {
        String userName = routingContext.getBodyAsJson().getString("userName");
        Integer roomNumber = routingContext.getBodyAsJson().getInteger("roomNumber");
        String roomPassword = routingContext.getBodyAsJson().getString("roomPassword");
        if (userName == null || roomNumber == null || roomPassword == null)
            throw new NullPointerException();
        return lobbyManager.joinRoom(userName, roomNumber, roomPassword);
    }
}
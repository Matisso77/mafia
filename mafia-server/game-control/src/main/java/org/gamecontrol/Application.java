package org.gamecontrol;

import io.vertx.core.Vertx;
import org.gamecontrol.core.ServerVerticle;

public class Application {

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new ServerVerticle());
    }
}

package org.gamecontrol.database;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class DatabaseMock {

    private static Logger LOGGER = LoggerFactory.getLogger(DatabaseMock.class);

    public String getUserSalt(String userName){
        LOGGER.info("Salt for: ",userName);
        return "mockedSalt";
    }

    public String getPassword(String userName){
        LOGGER.info("Password for: "+userName);
        return "mockedPassword";
    }

    public Boolean registerUser(String userName,String password,String salt,String email){
        String info = "User Registered!\n" +
                " Username: " + userName +
                "\n Password: " + password +
                "\n Salt: " + salt +
                "\n email: " + email+"\n";
        LOGGER.info(info);
        return !userName.equals("Wrong");
    }

    public Boolean checkEmail(String userName, String email){
        String info = "Checking Email for: " + userName;
        LOGGER.info(info);
        return !userName.equals("Wrong");
    }

}

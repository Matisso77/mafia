package org.gamecontrol.database;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import io.reactivex.Observable;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bson.Document;
import org.gamecontrol.core.Exceptions.PlayerExistsException;
import org.gamecontrol.core.Exceptions.UserNotFoundException;

import java.util.Objects;

import static com.mongodb.client.model.Filters.eq;

public class UserDataDatabase {

    private static Logger LOGGER = LoggerFactory.getLogger(UserDataDatabase.class);
    private MongoCollection<Document> userDataCollection;

    public UserDataDatabase(MongoClient mongoClient, String dbName) {
        MongoDatabase db = mongoClient.getDatabase(dbName);
        userDataCollection = db.getCollection("UserData");
        LOGGER.info("Connected to database: " + db.getName());
    }

    public void registerUser(String username, String password, String salt, String email) throws PlayerExistsException {
        if (findUser(username) == null) {
            addUser(username, password, salt, email);
            return;
        }
        LOGGER.info("User with name: " + username + " exists!");
        throw new PlayerExistsException();
    }

    private void addUser(String username, String password, String salt, String email) {
        Document user = new Document("username", username)
                .append("password", password)
                .append("salt", salt)
                .append("email", email);
        userDataCollection.insertOne(user);
        LOGGER.info("Added user: " + user.toJson());
    }

    public void deleteUser(String username) {
        userDataCollection.deleteOne(eq("username", username));
        LOGGER.info("Deleted user with name: " + username);
    }

    public Document findUser(String username) {
        return userDataCollection.find(eq("username", username)).first();
    }

    public JsonArray getAllUsers() {
        FindIterable<Document> users = userDataCollection.find();
        JsonArray jsonArray = new JsonArray();
        Observable.fromIterable(users)
                .map(Document::toJson)
                .map(JsonObject::new)
                .blockingForEach(jsonArray::add);
        return jsonArray;
    }

    public String getPassword(String username) throws UserNotFoundException {
        Document user = findUser(username);
        if(Objects.isNull(user)) {
            throw new UserNotFoundException();
        }
        return user.get("password").toString();
    }

    public void changePassword(String username, String password, String salt) {
        Document doc = new Document("password", password)
                .append("salt", salt);
        userDataCollection.updateOne(eq("username", username), new Document("$set", doc));
    }

    public String getUserSalt(String username){
        Document user = findUser(username);
        if(Objects.isNull(user))
            return "salt";
        return user.get("salt").toString();
    }

    public Boolean checkEmail(String username, String email){
        Document user = findUser(username);
        return email.equals(user.get("email").toString());
    }
}

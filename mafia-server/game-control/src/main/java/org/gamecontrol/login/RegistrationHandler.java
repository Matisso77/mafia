package org.gamecontrol.login;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.core.Exceptions.PlayerExistsException;
import org.gamecontrol.core.Exceptions.router.BadRequestHandler;
import org.gamecontrol.database.UserDataDatabase;

class RegistrationHandler {
    private final UserDataDatabase database;
    private final BadRequestHandler badRequestHandler;

    RegistrationHandler(UserDataDatabase database) {
        this.database = database;
        badRequestHandler = new BadRequestHandler();
    }

    void registerUser(RoutingContext routingContext) {
        try {
            putToDatabase(routingContext);
            JsonObject response = new JsonObject().put("Message", "Registration OK!");
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(200)
                    .end(response.toString());
        } catch (PlayerExistsException e) {
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(400)
                    .end(new JsonObject().put("Error", "User Exists!").toString());
        } catch (NullPointerException e) {
            badRequestHandler.handle(routingContext);
        }


    }

    private Boolean putToDatabase(RoutingContext routingContext) throws PlayerExistsException, NullPointerException {
        String username = routingContext.getBodyAsJson().getString("username");
        String password = routingContext.getBodyAsJson().getString("password");
        String salt = routingContext.getBodyAsJson().getString("salt");
        String email = routingContext.getBodyAsJson().getString("email");
        if (username == null || password == null || salt == null || email == null)
            throw new NullPointerException();
        database.registerUser(username, password, salt, email);
        return true;
    }
}

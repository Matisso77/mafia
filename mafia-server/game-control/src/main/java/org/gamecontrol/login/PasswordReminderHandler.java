package org.gamecontrol.login;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.database.UserDataDatabase;

class PasswordReminderHandler {

    private final UserDataDatabase database;

    private final int ERR_CODE = 400;

    PasswordReminderHandler(UserDataDatabase database) {
        this.database = database;
    }

    void remindPassword(RoutingContext routingContext) {
        if(checkInDatabase(routingContext)){
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(200)
                    .end(routingContext.getBodyAsString());
        } else {
            JsonObject responseJson = new JsonObject().put("ErrorMessage", "Invalid Username or Email!");
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(ERR_CODE)
                    .end(responseJson.toString());
        }
    }

    private boolean checkInDatabase(RoutingContext routingContext){
        String username = routingContext.getBodyAsJson().getString("username");
        String email = routingContext.getBodyAsJson().getString("email");
        if(username == null || email == null)
            return false;
        return database.checkEmail(username,email);
    }

}

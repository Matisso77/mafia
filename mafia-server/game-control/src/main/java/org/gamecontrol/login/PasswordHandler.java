package org.gamecontrol.login;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.core.Exceptions.UserNotFoundException;
import org.gamecontrol.database.UserDataDatabase;

class PasswordHandler {

    private final UserDataDatabase database;
    private final int ERR_CODE = 401;

    PasswordHandler(UserDataDatabase database) {
        this.database = database;
    }

    void authorize(RoutingContext routingContext) {
        String username = routingContext.getBodyAsJson().getString("username");
        if (validPassword(routingContext, username)) {
            JsonObject responseJson = new JsonObject().put("username", username);
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(200)
                    .end(responseJson.toString());
        } else {
            JsonObject responseJson = new JsonObject().put("ErrorMessage", "Invalid Login or Password!");
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(ERR_CODE)
                    .end(responseJson.toString());
        }
    }

    private Boolean validPassword(RoutingContext routingContext, String username) {
        String password = routingContext.getBodyAsJson().getString("password");
        if (password == null || username == null) return false;
        try {
            return password.equals(database.getPassword(username));
        } catch (UserNotFoundException e) {
            return false;
        }
    }
}

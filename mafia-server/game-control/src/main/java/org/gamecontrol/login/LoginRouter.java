package org.gamecontrol.login;

import com.mongodb.MongoClient;
import io.vertx.ext.web.Router;
import org.gamecontrol.database.UserDataDatabase;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public class LoginRouter {

    private Properties appProps = new Properties();
    private SaltHandler saltHandler;
    private PasswordHandler passwordHandler;
    private RegistrationHandler registrationHandler;
    private PasswordReminderHandler passwordReminderHandler;

    public LoginRouter() {
        loadProperties();
        MongoClient mongoClient = new MongoClient();
        UserDataDatabase database = new UserDataDatabase(mongoClient, appProps.getProperty("dbName"));
        saltHandler = new SaltHandler(database);
        passwordHandler = new PasswordHandler(database);
        registrationHandler = new RegistrationHandler(database);
        passwordReminderHandler = new PasswordReminderHandler(database);
    }

    public Router setLoginApiRouting(Router router) {
        router.post("/api/login/salt").handler(saltHandler::getSalt);
        router.post("/api/login/password").handler(passwordHandler::authorize);
        router.post("/api/login/register").handler(registrationHandler::registerUser);
        router.post("/api/login/forget").handler(passwordReminderHandler::remindPassword);
        return router;
    }

    private void loadProperties() {
        String rootPath = Objects.requireNonNull(
                Thread.currentThread()
                        .getContextClassLoader()
                        .getResource(""))
                .getPath();
        try {
            appProps.load(new FileInputStream(rootPath + "Database.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

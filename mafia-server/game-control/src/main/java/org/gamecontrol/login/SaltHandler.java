package org.gamecontrol.login;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.gamecontrol.database.UserDataDatabase;

class SaltHandler {
    private final UserDataDatabase database;

    SaltHandler(UserDataDatabase database) {
        this.database = database;
    }


    void getSalt(RoutingContext routingContext) {
        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(200)
                .end(getSaltResponse(routingContext));
    }

    private String getSaltResponse(RoutingContext routingContext) {
        String username = routingContext.getBodyAsJson().getString("username");
        if(username == null)
            return new JsonObject().put("ERROR","Invalid Request!").toString();
        return new JsonObject()
                .put("username", username)
                .put("salt", database.getUserSalt(username))
                .toString();
    }
}

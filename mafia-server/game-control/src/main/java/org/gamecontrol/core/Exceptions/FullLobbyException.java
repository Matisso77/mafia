package org.gamecontrol.core.Exceptions;

public class FullLobbyException extends Exception {

    public FullLobbyException() {
        super("Lobby is Full!");
    }
}
package org.gamecontrol.core.Exceptions.router;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class BadRequestHandler {

    private final static int ERR_CODE = 400;

    public void handle(RoutingContext routingContext) {
        JsonObject response = new JsonObject().put("Message", "Bad Request!");
        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(ERR_CODE)
                .end(response.toString());
    }
}

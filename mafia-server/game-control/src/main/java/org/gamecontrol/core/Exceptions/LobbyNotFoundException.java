package org.gamecontrol.core.Exceptions;

public class LobbyNotFoundException extends Exception {
    public LobbyNotFoundException() {
        super("Lobby not found!");
    }
}

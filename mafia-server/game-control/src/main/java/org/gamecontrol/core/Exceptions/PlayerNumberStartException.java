package org.gamecontrol.core.Exceptions;

public class PlayerNumberStartException extends Exception {
    public PlayerNumberStartException() {
        super("Not enough players in room to start game!");
    }
}

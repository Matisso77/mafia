package org.gamecontrol.core.Exceptions;

public class PlayerExistsException extends Exception{

    public PlayerExistsException() {
        super("Player Exists!");
    }
}

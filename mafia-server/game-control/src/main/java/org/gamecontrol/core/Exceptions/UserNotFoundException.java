package org.gamecontrol.core.Exceptions;

public class UserNotFoundException extends Exception {

    public UserNotFoundException() {
        super("Player NotExists!");
    }
}

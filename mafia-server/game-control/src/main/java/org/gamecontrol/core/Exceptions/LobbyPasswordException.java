package org.gamecontrol.core.Exceptions;

public class LobbyPasswordException extends Exception {
    public LobbyPasswordException() {
        super("Wrong Lobby Password!");
    }
}

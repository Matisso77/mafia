package org.gamecontrol.core;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.gamecontrol.lobby.business.LobbyManager;
import org.gamecontrol.lobby.router.LobbyRouter;
import org.gamecontrol.login.LoginRouter;

import java.io.IOException;

public class ServerVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> fut) throws IOException {
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.get("/api/alive").handler(this::alive);
        LoginRouter loginApiRouter = new LoginRouter();
        LobbyManager lobbyManager = new LobbyManager();
        LobbyRouter lobbyRouter = new LobbyRouter(lobbyManager);
        loginApiRouter.setLoginApiRouting(router);
        lobbyRouter.setLobbyApiRouting(router);
        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(8080, result -> {
                    if (result.succeeded()) {
                        fut.complete();
                    } else {
                        fut.fail(result.cause());
                    }
                });
    }

    private void alive(RoutingContext rc) {
        JsonObject responseJson = new JsonObject().put("Alive", "OK");
        rc.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(200)
                .end(responseJson.toString());
    }
}


package org.gamecontrol.lobby.business;

import org.gamecontrol.CommunicationModule;
import org.gamecontrol.core.Exceptions.FullLobbyException;
import org.gamecontrol.core.Exceptions.LobbyNotFoundException;
import org.gamecontrol.core.Exceptions.LobbyPasswordException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.core.IsEqual.equalTo;

public class LobbyManagerTest {

//
//    private final static String OWNER = "owner";
//    private final static String PLAYER = "player";
//    private final static String PASSWORD = "password";
//    private final static int SIZE = 2;
//    @Rule
//    public ErrorCollector collector = new ErrorCollector();
//    private Lobby created;
//    private LobbyManager lobbyManager;
//    private int roomNumber;
//
//    @Before
//    public void setUp() throws LobbyNotFoundException, IOException {
//        lobbyManager = new LobbyManager();
//        roomNumber = lobbyManager.createRoom(PASSWORD, SIZE, OWNER);
//        created = lobbyManager.getLobby(roomNumber);
//    }
//
//    @Test
//    public void createRoomTest() {
//        collector.checkThat(created.getId(), equalTo(roomNumber));
//        collector.checkThat(created.getPassword(), equalTo(PASSWORD));
//        collector.checkThat(created.getPlayerList().contains(OWNER), equalTo(true));
//        collector.checkThat(created.getSize(), equalTo(SIZE));
//    }
//
//    @Test
//    public void joinRoomTest() throws FullLobbyException, LobbyPasswordException, LobbyNotFoundException {
//        lobbyManager.joinRoom(PLAYER, roomNumber, PASSWORD);
//        boolean isInLobby = lobbyManager.getLobby(roomNumber).getPlayerList().contains(PLAYER);
//        assertTrue(isInLobby);
//    }
//
//    @Test
//    public void roomExistsTest() throws LobbyNotFoundException {
//        boolean inLobby = lobbyManager.roomExists(OWNER, roomNumber);
//        boolean roomNotExists = lobbyManager.roomExists(OWNER, roomNumber + 1);
//        boolean playerNotIn = lobbyManager.roomExists(PLAYER, roomNumber);
//        collector.checkThat(inLobby, equalTo(true));
//        collector.checkThat(roomNotExists, equalTo(false));
//        collector.checkThat(playerNotIn, equalTo(false));
//    }
//
//    @Test(expected = LobbyNotFoundException.class)
//    public void deleteRoomWithTest() throws LobbyNotFoundException {
//        lobbyManager.deleteRoom(roomNumber);
//        lobbyManager.getLobby(roomNumber);
//    }
//
//
//    @Test
//    public void getPlayerListTest() throws LobbyNotFoundException {
//        List<String> actual = lobbyManager.getPlayerList(roomNumber);
//        List<String> expected = new LinkedList<>();
//        expected.add(OWNER);
//        assertEquals(expected, actual);
//    }
//
//    @Test(expected = LobbyNotFoundException.class)
//    public void leaveRoomWith1Player() throws LobbyNotFoundException {
//        lobbyManager.leaveRoom(roomNumber, OWNER);
//        lobbyManager.getLobby(roomNumber);
//    }
//
//    @Test
//    public void leaveRoomWith2Players() throws LobbyNotFoundException, FullLobbyException, LobbyPasswordException {
//        lobbyManager.joinRoom(PLAYER, roomNumber, PASSWORD);
//        lobbyManager.leaveRoom(roomNumber, OWNER);
//        boolean playerExists = lobbyManager.getLobby(roomNumber).getPlayerList().contains(PLAYER);
//        boolean ownerLeftRoom = lobbyManager.getLobby(roomNumber).getPlayerList().contains(OWNER);
//        collector.checkThat(playerExists, equalTo(true));
//        collector.checkThat(ownerLeftRoom, equalTo(false));
//    }
}
namespace game{
    enum Role{
        Peasant,    //does nothing
        Cop,        //checks target's allegiance (faction) / one person only
        Hoe,        //protects the target from dying that night / one person only (Protected status)
        Mobster     //votes with other mobsters on who to kill during the night
    };

    enum Faction{
        City,
        Mafia
    };
    
    enum Status{
        Alive,
        Dead
    };

    enum Modifier{
        none,
        Protected   //status applied by a Hoe
    };
    
}
namespace game{
class Player{
    private string name;
    public Role role;
    public Faction faction;
    public Status status = Status.Alive;
    public Modifier modifier = Modifier.none;
    public Player(string name, Role r){
        this.name = name;
        this.role = r;
        if (r != Role.Mobster){ //TODO: Expand the options
            this.faction = Faction.City;
        }
        else{
            this.faction = Faction.Mafia;
        }
    }

    internal string getName(){
        return this.name;
    }

    internal Role getRole(){
        return this.role;
    }

    internal void setRole(Role r){ //it likely will not be used, but there may be scenarios that require employing this method
        this.role = r;
    }

    internal Faction getFaction(){
        return this.faction;
    }

    internal void setFaction(Faction f){ //look 8 lines up
        this.faction = f;
    }

    internal Modifier getModifier(){
        return this.modifier;
    }

    internal void setModifier(Modifier m){
        this.modifier = m;
    }

    internal Status GetStatus(){
        return this.status;
    }

    internal void setStatus(Status s){
        this.status = s;
    }
        
}
}
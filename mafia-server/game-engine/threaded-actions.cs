using System;
using System.Threading;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;
namespace game{
    internal class ThreadedActions{
        // List<Thread> gamesThreads = new List<Thread>();
        List<Game> games = new List<Game>();


        internal void threadedAction(ref Communication comms, ref ThreadManager threadMan, String[] args){
            switch (args[1]){
                case "C": createGame(ref comms, ref threadMan, args);
                        break;
                case "E": endGame(ref threadMan, Int32.Parse(args[0]));
                        break;
                case "K": findGame(Int32.Parse(args[0])).updateVotes(args[3], "K");
                        break;
                case "R": findGame(Int32.Parse(args[0])).updateVotes(args[3], "R");
                        break;
                case "A": findGame(Int32.Parse(args[0])).anvil(args[2]);
                        break;
                default: Console.WriteLine("Wrong comm! " + args[1]);
                    break;
            }
        }

        void createGame(ref Communication comms, ref ThreadManager threadMan, String[] arrAction){
            threadMan.registerThread(Thread.CurrentThread, Int32.Parse(arrAction[0]));
            List<Player> players = new List<Player>();
            String playersMsg = "";
            //TODO: Implement custom scenarios
            StreamReader readStream = new StreamReader("scenarios/default.json");
            var read = readStream.ReadToEnd();
            var json = JObject.Parse(read);
            List<String> roles = json.GetValue(arrAction[3]).ToObject<List<String>>();
            roles.Shuffle(); 
            

            foreach (String uid in arrAction[2].Split(':')){
                playersMsg += uid + ","+ roles[0] + ":";
                Enum.TryParse(roles[0], out Role r);
                players.Add(new Player(uid, r)); //TO-DO
                roles.RemoveAt(0);
            }
            Int32 gid = Int32.Parse(arrAction[0]);
            Game g = new Game(players, gid, ref comms);
            lock(games){
                games.Add(g);
            }

            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " (" + Int32.Parse(arrAction[0]) + ")[INFO]: Game created.");
            comms.sendMessage(gid + ";P;" + playersMsg.Remove(playersMsg.Length - 1));
            g.gameLoop();
            endGame(ref threadMan, gid);
        }

        void endGame(ref ThreadManager threadMan, Int32 gid){
            lock(games){
                Game tmpGame = findGame(gid);
                if(tmpGame != null){
                    // ThreadManType tBag = threadMan.threads.Find(x => x.gid == gid);
                    // tBag.t.Abort();
                    // threadMan.threads.Remove(tBag);
                    games.Remove(tmpGame);
                    tmpGame = null;
                }
            }
            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " (" + gid + ")[INFO]: Game ended.");
        }

        private Game findGame(Int32 gid){
            return games.Find(x => x.getID() == gid);
        }
    }
    static class MyExtensions{
        private static Random rng = new Random((int)DateTime.Now.Ticks);  
        public static void Shuffle<T>(this IList<T> list){  
            int n = list.Count;  
            while (n > 1) {  
                n--;  
                int k = rng.Next(n + 1);  
                T value = list[k];  
                list[k] = list[n];  
                list[n] = value;  
            }  
        }
    }
}


//Creation of the file for testing purposes
using System;
using System.Threading;
using System.Collections.Generic;
namespace game{
    static class States{
        //send,receive and process messages; perform appropriate functions
        internal static void start(ref List<Player> players, Game game){
            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")+" ("+game.getID()+")[INFO]: Game initiated");
            Thread.Sleep(2000);
        }

        internal static void firstNight(ref List<Player> players, Game game){
            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")+" ("+game.getID()+")[INFO]: First night initiated");
            Thread.Sleep(10000);
        }

        internal static void firstDay(ref List<Player> players, Game game){
            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")+" ("+game.getID()+")[INFO]: First day initiated");
            game.day++;
            game.mike.sendMessage(game.getID()+";S;DAY;"+game.day);
            game.dayVote(players);
        }

        internal static void night(ref List<Player> players, Game game){
            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")+" ("+game.getID()+")[INFO]: Night "+game.day+" initiated");
            game.mike.sendMessage(game.getID()+";S;NIGHT;"+game.day);
            List<Role> voters = game.getVoters(players);
            if (voters == null){
                game.endGame();
            }
            while(voters.Count != 0){
                game.nightVote(players, voters[0]);
                voters.RemoveAt(0);

            }
            if (!game.endCheck()){
                States.day(ref players, game);
            }
        }

        internal static void day(ref List<Player> players, Game game){
            foreach(Player p in players){
                if (p.getModifier() == Modifier.Protected){ //remove one-night modifiers
                    p.setModifier(Modifier.none);
                }
            }
            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")+" ("+game.getID()+")[INFO]: Day "+game.day+" initiated");
            game.day++;
            game.mike.sendMessage(game.getID()+";S;DAY;"+game.day);
            game.dayVote(players);
            if (!game.endCheck()){
                States.night(ref players, game);
            }
        }
    }
}
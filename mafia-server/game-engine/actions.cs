using System;
using System.Threading;
using System.Collections.Generic;
namespace game{
    partial class Game{
        Random rnd = new Random((int)DateTime.Now.Ticks);
        internal Dictionary<Role,int> roleNums = new Dictionary<Role,int>(){
            { Role.Cop, 101 },
            { Role.Hoe, 102 },
            { Role.Mobster, 200 }
        };//TOEXPAND: add more roles


        internal void dayVote(List<Player> players){
            string tmp = "";
            foreach (Player p in players){
                tmp+=p.getName()+":";
            }
            tmp=tmp.Substring(0,tmp.Length-1); //peel the last colon character
            string msg = this.GID+";V;"+tmp+";"+tmp;
            this.mike.sendMessage(msg);
            this.flag = true;
            void start(){
                this.votes.Clear();
                this.voted = 0;
            }
            void wait(){
                System.Threading.SpinWait.SpinUntil( () => this.voted == players.Count, 1000*this.getTimeouts().Item1+3000);
                //Thread.Sleep(1000*this.getTimeouts().Item1+3000);
                this.flag = false;
            }
            void act(){
                this.votes.Sort((x,y) => y.counter.CompareTo(x.counter));//TODO: Make sure there are no players with equal votes
                foreach (Vote v in votes){
                    Console.WriteLine(v.player + ", " + v.counter);
                }
                if (votes.Count == 0){ //Actually no votes; create a random singular vote 
                    int index = rnd.Next(players.Count);
                    int choice = rnd.Next(2);
                    String[] choices = {"k", "r"};
                    votes.Add(new Vote(players[index].getName(), choices[choice], 1));
                    Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")+" ("+this.getID()+")[WARNING]: Day "+this.day+": No votes cast. Randomly casting "+choices[choice]+" on "+players[index].getName());
                }
                if (votes[0].action.ToLower()[0] == 'k'){//kill
                    try{
                        this.kill(players.Find(x => x.getName() == votes[0].player));
                    }
                    catch(NullReferenceException e){
                        Console.Error.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")+" ("+this.getID()+")[ERROR]: NoPlayerException (kill): Player "+votes[0].player);
                    }
                } 
                else {//reveal
                    try{
                        this.reveal(players.Find(x => x.getName() == votes[0].player));
                    }
                    catch(NullReferenceException e){
                        Console.Error.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")+" ("+this.getID()+")[ERROR]: NoPlayerException (reveal): Player "+votes[0].player);
                    }
                }
            this.votes.Clear();
            this.voted = 0;
            }
            start();
            wait();
            act();
        }

        internal void nightVote(List<Player> players, Role r){
            string arg1="";
            string arg2="";
            foreach (Player p in players){
                if (p.getRole() == r){
                    arg1+=p.getName()+":";
                } else {
                    arg2+=p.getName()+":";
                }
            }
            arg1=arg1.Substring(0,arg1.Length-1); //peel the last colon character
            arg2=arg2.Substring(0,arg2.Length-1); //peel the last colon character
            string msg = this.GID+";N;"+roleNums[r].ToString()+";"+arg1+";"+arg2;
            this.mike.sendMessage(msg);
            this.flag = true;
            void start(){
                this.votes.Clear();
                this.voted = 0;
            }
            void wait(){
                System.Threading.SpinWait.SpinUntil( () => this.voted == players.Count, 1000*this.getTimeouts().Item2+3000);
                //Thread.Sleep(1000*this.getTimeouts().Item2+3000);
                this.flag = false;
            }
            void act(){
                this.votes.Sort((x,y) => y.counter.CompareTo(x.counter));//TODO: Make sure there are no players with equal votes
                if (votes.Count == 0){ //Actually no votes; create a random singular vote 
                    String[] arr = arg2.Split(":");
                    int index = rnd.Next(arr.Length);
                    votes.Add(new Vote(arr[index], "NIGHT", 1));
                    Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")+" ("+this.getID()+")[WARNING]: Night "+this.day+": No votes cast. Randomly casting role "+r.ToString()+" vote on player "+arr[index]);//players[index].ToString());
                }
                Player target = players.Find(x => x.getName() == votes[0].player);
                switch(r){
                    case Role.Mobster:
                        this.kill(target);
                        break;
                    case Role.Hoe:
                        target.setModifier(Modifier.Protected);
                        break;
                    case Role.Cop:
                        String getVerdict(Player p) => p.getRole() == Role.Mobster ? "TRUE" : "FALSE";//TODO:Modify as more classes get implemented
                        msg = this.GID+";T;"+target.getName()+";"+ getVerdict(target);
                        mike.sendMessage(msg);
                        break;
                    //TODO: ADD MORE ROLES
                    default:
                        break;
                }
                this.votes.Clear();
                this.voted = 0;
            }
            start();
            wait();
            act();
        }
    }
}
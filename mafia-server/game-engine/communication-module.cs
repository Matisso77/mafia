using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
namespace game{
    class Communication{
        internal Socket clientSocket = null;

        public Communication(){
            IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 4343);
            clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            clientSocket.Connect(serverAddress);
        }
        
        ~Communication(){
            clientSocket.Close();
        }

        internal void sendMessage(string message){
            int messageLen = System.Text.Encoding.ASCII.GetByteCount(message);
            byte[] messageBytes = System.Text.Encoding.ASCII.GetBytes(message);
            byte[] messageLenBytes = System.BitConverter.GetBytes(messageLen);
            clientSocket.Send(messageLenBytes);
            clientSocket.Send(messageBytes);
        }

        internal string receive(){
            byte[] rcvLenBytes = new byte[4];
            clientSocket.Receive(rcvLenBytes);
            int rcvLen = System.BitConverter.ToInt32(rcvLenBytes, 0);
            byte[] rcvBytes = new byte[rcvLen];
            clientSocket.Receive(rcvBytes);
            String answer = System.Text.Encoding.ASCII.GetString(rcvBytes);
            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "[SERVER ECHO]: " + answer);
            return answer;
        }
    }
}
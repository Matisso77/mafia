using System;
using System.Threading;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;
namespace game{
    internal class Vote{
        internal string player;
        internal string action;
        internal int counter;

        internal Vote(string player, string action, int counter){
            this.player = player;
            this.action = action;
            this.counter = counter;
        }
    }

    partial class Game{
        internal List<Player> players = new List<Player>();
        internal List<Player> corpses = new List<Player>();
        internal int GID;
        internal int day = 1;
        internal int tDay = 30;
        internal int tNight = 30;
        internal int voted = 0;
        internal bool flag = true;
        //internal List actions;
        internal List<Vote> votes = new List<Vote>();
        internal Dictionary<Faction, int> factions = new Dictionary<Faction, int>(); //populated by data from $GID.json or whatever
        internal Communication mike;
        
        public Game(List<Player> players, int GID, ref Communication mike){ 
            //settings stored in $GID.json
            this.players = players;
            this.GID = GID;
            this.mike = mike;
            factions.Add(Faction.City, 0);
            factions.Add(Faction.Mafia, 0);
            foreach (Player p in players){
                factions[p.getFaction()] += 1;
            }
            String path = "games/"+GID+".json";
            if(File.Exists(path)){
                using (StreamReader r = new StreamReader(path)){
                    var json = JObject.Parse(r.ReadToEnd());
                    this.tDay = json.GetValue("timeouts")[0].ToObject<int>();
                    this.tDay = json.GetValue("timeouts")[1].ToObject<int>();
                }
            }
            //read timeouts from json.
        }

        ~Game(){
            players.Clear();
            corpses.Clear();
        }

        internal void gameLoop(){
            States.start(ref players, this);
            States.firstNight(ref players, this);
            States.firstDay(ref players, this);
            // while(true){
                States.night(ref players, this);
            //     endCheck();
            //     States.day(ref players, this);
            //     endCheck();
            // }
        }

        internal void endGame(){
            mike.sendMessage(this.getID()+";E;");
        }

        internal void gameWon(int status){
            string msg = "";
            if(status == 1){
                msg = "The Mafia has won!";
            }
            else if (status == 2){
                msg = "The City has been victorious!";
            }
            mike.sendMessage(this.getID()+";W;"+msg);
            this.endGame();
        }

        internal bool endCheck(){
            if (factions[Faction.City] <= factions[Faction.Mafia]){
                gameWon(1);
                return true;
            }
            else if(factions[Faction.Mafia] == 0){
                gameWon(2);
                return true;
            }
            return false;
        }

        internal void kill(Player target){
            if (target.getModifier() == Modifier.Protected){
                //notify and act accordingly
                target.setModifier(Modifier.none);
            }
            else{
                //remove the player from any queues/lists 
                //check whether the player's death does not involve any additional actions
                //et cetera
                factions[target.getFaction()]--;
                target.setStatus(Status.Dead);
                players.Remove(target);
                corpses.Add(target); 
                string msg = this.GID+";K;"+target.getName()+";"+target.getRole().ToString();
                this.mike.sendMessage(msg);      
            }
        }

        internal void reveal(Player target){
            //possibly add some cases
            string msg = this.GID+";R;"+target.getName()+";"+target.getRole().ToString();
            this.mike.sendMessage(msg);

        }

        internal int getID(){
            return this.GID;
        }

        internal Tuple<int, int> getTimeouts(){
            return Tuple.Create(this.tDay, this.tNight);
        }

        internal List<Role> getVoters(List<Player> players){
            List<Role> all = new List<Role>{Role.Cop,Role.Hoe,Role.Mobster}; //<EXPANDABLE> TODO: Expand as the number of voting roles increases
            List<Role> possible = new List<Role>{}; //<EXPANDABLE> TODO: Expand as the number of voting roles increases
            foreach(Role r in all){
                if(players.Find(x => x.getRole() == r) != null){
                    possible.Add(r);
                }
            }
            return possible;
        }


        //Thread functions

        internal void updateVotes(string player, string action){
            voted += 1;
            lock(votes){
                foreach(Vote v in votes){
                    if ((v.player == player) && (v.action == action)){
                        v.counter += 1;
                        return;
                    }
                } 
                votes.Add(new Vote(player, action, 1));
            }
            return;
        }

        internal void anvil(string player){
            kill(players.Find(x => x.getName() == player));
        }
    }
}
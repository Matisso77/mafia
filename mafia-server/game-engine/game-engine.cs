using System;
using System.Threading;
using System.Collections.Generic;
namespace game{
    class Program{
        static void Main(string[] args){
            Communication comms = new Communication();
            ThreadedActions actionCentre = new ThreadedActions();
            ThreadManager threadMan = new ThreadManager();
            Thread tMan = new Thread(threadMan.threadMonitor);
            tMan.Start();

            // run continuously, connect with server, create games, support game-server connection
            while (true) {
                String[] arrAction = comms.receive().Split(';');
                if (!(arrAction.Length < 2)){
                    Thread t = new Thread(() => actionCentre.threadedAction(ref comms, ref threadMan, arrAction));
                    t.Start();
                }
            }
        }
    }
}
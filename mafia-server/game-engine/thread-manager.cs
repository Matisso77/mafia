using System;
using System.Threading;
using System.Collections.Generic;
namespace game{
    internal class ThreadManType{
        public ThreadManType(ref Thread t, int gid){
            this.t = t;
            this.gid=gid;
        }
        internal Thread t;
        internal int gid;
    }

    internal class ThreadManager{
        internal List<ThreadManType> threads = new List<ThreadManType>();

        internal void registerThread(Thread t, int gid){
            lock(threads){
                threads.Add(new ThreadManType(ref t, gid));
            }
        }

        internal void threadMonitor(){
            while (true){
                int threadsAlive = 0;
                lock(threads){
                    List<ThreadManType> deadThreads = new List<ThreadManType>();
                    foreach (ThreadManType tmt in threads){
                        if (tmt.t.IsAlive)
                            threadsAlive += 1;
                        else 
                            deadThreads.Add(tmt);
                    }
                    while (deadThreads.Count > 0){
                        threads.Remove(deadThreads[0]);
                        deadThreads[0] = null;
                        deadThreads.RemoveAt(0);
                    }
                }
                Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " [THREAD MANAGER]: Threads alive: " + threadsAlive + ", array size: " + threads.Count);
                Thread.Sleep(10000);
            }
        }
    }
}
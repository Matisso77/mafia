<?php
session_start();
if(!isset($_SESSION['login'])) {
    header("location: ./");
    exit();
}
if(isset($_SESSION['room1']) && isset($_SESSION['room2'])&& isset($_SESSION['room3'])) {
    header("location: lobby.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Mafia: Abandoned game</title>
    <link rel="icon" href="./img/j.png" type="image/png" sizes="16x16">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/lobby.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono" rel="stylesheet">
</head>
<body>
<header>
    <iframe src="audio/silence.mp3" id="musicframe"></iframe>
    <audio id="audio" autoplay controls loop><source src="audio/sound.mp3" type="audio/mp3"></audio>
</header>

<main>
    <h1>Hi, <?php echo $_SESSION['login']?>!</h1>
    <p>Game has been abandoned..</p>
    <div class="link-wrapper">
        <a href="lobby.php">Return to lobby</a>
    </div>
</main>
<aside>
    <div id="sound"></div>
    <img id="background" src="img/intro1.jpg" alt="background">
</aside>
<footer>
</footer>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/music.js"></script>
<script src="js/script.js"></script>
</body>
</html>
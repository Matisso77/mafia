<?php
session_start();
include("./utils/utilities.php");
if(!isset($_SESSION['login'])) {
    header("location: ./");
    exit();
}
$flag=0;
if(!isset($_SESSION['room1']) || !isset($_SESSION['room2']) || !isset($_SESSION['room3'])) {
    $flag++;
}
if(!isset($_SESSION['gamenr'])) {
    $flag++;
}
if($flag != 1) {
    unset($_SESSION['room1']);
    unset($_SESSION['room2']);
    unset($_SESSION['room3']);
    unset($_SESSION['gamenr']);
    header("location: lobby.php");
    exit();
}
if(!isset($_SESSION['gamenr'])) {
    $_SESSION['gamenr'] = $_SESSION['room1'];
    unset($_SESSION['room1']);
    unset($_SESSION['room2']);
    unset($_SESSION['room3']);
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Mafia: Game</title>
    <link rel="icon" href="./img/j.png" type="image/png" sizes="16x16">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/lobby.css" type="text/css" />
    <link rel="stylesheet" href="css/game.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono" rel="stylesheet">
</head>
<body>
<header>
</header>

<main>
    <h1>The game</h1>
    <section class="game">
        <p id="info1"></p>
        <p id="info2"></p>
        <section class="chooser" id="chooser">
        </section>
    </section><!--
    --><section class="users">
        <h1>Users</h1>
        <div id="game-players">
        </div>
        <p class="role"></p>
        <span id="role-image"></span>
    </section>
    <a href="forms/exit.php">Exit the game</a>
</main>
<aside>
    <img id="background" src="img/intro1.jpg" alt="background">
</aside>
<footer>
</footer>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/script.js"></script>
<script>
    function formsubmit(src) {
        $.post({
            type: "POST",
            url: "forms/event.php",
            data: {
                target: src
            },
        }).always(function(text) {
            $("#chooser").hide();
        });
    }

    $(function() {
        $("#chooser").hide();
        sayHi();
        sayHi2();
        sayRole();

        setInterval(sayHi,1000);
        setInterval(sayHi2,1000);
        var x = setInterval(sayRole,1000);

        function sayHi() {
            $.post({
                type: "POST",
                url: "forms/gameping.php",
                data: {},
            }).always(function(text) {
                console.log("1// Jest teraz czas: " + new Date().getHours() + ":" + new Date().getMinutes() + ":"+new Date().getSeconds());
                var result = JSON.parse(text);
                if(result.dead !== null && result.dead !== 'null' && result.dead !== '') {
                    $("#info2").html(result.dead);
                }
                if(result.me === 'true') {
                    $("#info1").html('<span style="color: red;">You are dead!</span>');
                }
                $("#game-players").html(result.code);
            });
        }
        function sayHi2() {
            console.log("2// Jest teraz czas: " + new Date().getHours() + ":" + new Date().getMinutes() + ":"+new Date().getSeconds());
            $.post({
                type: "POST",
                url: "forms/gamepingvote.php",
                data: {},
            }).always(function(text) {
                if(text !== null && text !== 'null') {
                    var res = JSON.parse(text);
                    if(res.h1 !== null && res.h1 !== '') {
                        $("main > h1").html(res.h1);
                    }
                    if(res.voting !== null && res.voting !== '' && res.voting !== 'already') {
                        $("#chooser").show();
                        $("#chooser").html(res.voting);
                    } else if(res.voting === 'already') {
                    } else {
                        $("#chooser").hide();
                    }
                    if(res.p !== null && res.p !== '' && $("#info1").html() !== res.p) {
                        $("#info1").html(res.p);
                    }
                }
            });
        }
        function sayRole() {
            $.post({
                type: "POST",
                url: "forms/getrole.php",
                data: {},
            }).always(function(text) {
                if(text === "null" || text === null) {
                    $("p.role").html("Waiting for a role..");
                } else {
                    clearInterval(x);
                    $("p.role").html(text);
                    var trace = 'img/' + text + '.png';
                    $("#role-image").html('<img src="' + trace + '" alt="photo" class="role-photo"/>');
                }
            });
        }
    });
</script>
</body>
</html>

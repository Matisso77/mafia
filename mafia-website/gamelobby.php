<?php
include("utils/utilities.php");
session_start();

if(!isset($_SESSION['login'])) {
    header("location: ./");
    exit();
}
if(!(isset($_SESSION['room1']) && isset($_SESSION['room2'])&& isset($_SESSION['room3']))) {
    header("location: lobby.php");
    exit();
}
if(isset($_SESSION['gamenr'])) {
    header("location: game.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Mafia: Game lobby</title>
    <link rel="icon" href="./img/j.png" type="image/png" sizes="16x16">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/lobby.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono" rel="stylesheet">
</head>
<body>
<header>
    <iframe src="audio/silence.mp3" id="musicframe"></iframe>
    <audio id="audio" autoplay controls loop><source src="audio/sound.mp3" type="audio/mp3"></audio>
</header>

<main>
    <h1>Room <?php echo $_SESSION['room1']; ?></h1>
    <p>Password: <?php echo $_SESSION['room2']; ?></p>
    <p>Connected players: <?php echo '<span id="p-amount"></span>'; echo '/<span id="max-amount">'.$_SESSION['room3']; ?></span></p>
    <p class="warning" id="lessplayers"><?php utilities::show("errorik"); ?></p>
    <div class="link-wrapper">
        <a href="forms/startgame.php">Start the game</a>
    </div>
    <a href="forms/abandongame.php">Abandon</a>
</main>
<aside>
    <div id="sound"></div>
    <img id="background" src="img/intro1.jpg" alt="background">
</aside>
<footer>
</footer>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/music.js"></script>
<script src="js/script.js"></script>
<script>
    $(function() {
        sayHi();
        setInterval(sayHi,1000);

        function sayHi() {
            $.post({
                type: "POST",
                url: "forms/playerlist.php",
                data: {},
            }).always(function(text) {
                var myString = text;
                var myRegexp = /(.*):(.*)/g;
                var match = myRegexp.exec(myString);

                if(match[1] === "true") {
                    $("#p-amount").html(match[2]);
                    location.href = "game.php";
                } else {
                    $("#p-amount").html(match[2]);
                }
            });
        }
    });


</script>
</body>
</html>
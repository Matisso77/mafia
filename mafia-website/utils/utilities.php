<?php

class utilities
{

    public static function post($url, array $jsonData) {
        $ch = curl_init($url);
        $jsonDataEncoded = json_encode($jsonData);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        return curl_exec($ch);
    }

    public static function checkUser($user) {
        if(!preg_match('/^[A-Za-z0-9_\-]{4,16}$/', $user)) {
            return false;
        }
        return $user;
    }

    public static function checkMail($mail) {
        $regex = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
        if(!preg_match($regex, $mail)) {
            return false;
        }
        return $mail;
    }

    public static function checkPass($pass1, $pass2) {
        if($pass1 != $pass2) {
            return false;
        }
        if(strlen($pass1) < 8) {
            return false;
        }
        return $pass1;
    }

    public static function sendNewPassword($mail, $user, $newpass) {
        $to      = $mail;
        $subject = 'New password - Mafia';
        $message = '<h1>Hi '.$user.'!</h1>
        <p>You sent us a change password request, here it\'s your new password for our game: '.$newpass.'</p>
        <p>Change it just after logging in.</p>
        <p>Greetings, MafiaSQ(linjection)D.</p>';
        $headers = 'From: Jateusz Machniak'. "\r\n" .
            "Reply-To: Jateusz Machniak". "\r\n".
            "Return-Path: Jateusz Machniak". $to ."\r\n".
            "Content-Type: text/html; charset=UTF-8" . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);
    }

    public static function show($var) {
        if(isset($_SESSION[$var])) {
            echo $_SESSION[$var];
            unset($_SESSION[$var]);
        }
    }

    public static function generateSalt($it){
        $randString="";

        for($i=0; $i < $it; $i++){
            $randChar = utilities::getBase62Char(mt_rand(0,61));
            $randString .= $randChar;
        }

        return $randString;
    }

    private static function getBase62Char($num) {
        return substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", $num, 1);
    }

    public static function regexMsgGameEngine($msg) {
        preg_match_all('/([^;\s]+)/', $msg, $matches);
        $arr = $matches[count($matches)-1];

        preg_match_all('/([^:]+)/', $arr[2], $tmp);
        $arr2 = $tmp[count($tmp)-1];
        preg_match_all('/([^:]+)/', $arr[3], $tmp);
        $arr3 = $tmp[count($tmp)-1];

        return array($arr[0], $arr[1], $arr2, $arr3);
    }

    public static function regexMsgServer($msg) {
        preg_match_all('/([^:\s]+)/', $msg, $matches);
        return $matches[count($matches)-1];
    }
    public static function regexMsgServer2($msg) {
        preg_match_all('/([^:;\s]+:[^;\s]+)/', $msg, $matches);
        $arr = $matches[count($matches)-1];
        $new = array();
        for($i=0; $i<count($arr); ++$i) {
            preg_match_all('/([^:]+)/', $arr[$i], $tmp);
            $arr2 = $tmp[count($tmp)-1];
            $new[$i] = array();
            $new[$i][] = $arr2[0];
            $new[$i][] = $arr2[1];
        }
        return $new;
    }

    public static function makeVoting($title, $players) {
        $playerz = self::regexMsgServer($players);
        $text = "";
        $text.= '<h2>'.$title.'</h2><form id="event-click" action="forms/event.php" method="post">';
        for($i=0; $i<count($playerz); ++$i) {
            $text .= '<div class="option" onclick="formsubmit(this.innerHTML);">'.$playerz[$i].'</div>';
        }
        $text.="</form>";
        return $text;
    }
}
function setCookie(c_name,value,exdays)
{
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name)
{
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++)
    {
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x === c_name)
        {
            return unescape(y);
        }
    }
    return false;
}

var song = document.getElementsByTagName('audio')[0];
var played = getCookie('isPlaying');
var tillPlayed = getCookie('playTime');
var interval;

firstUpdate();

function firstUpdate() {
    if(played === "undefined" || played === "false" || played === false) {
        setCookie('isPlaying', "false");
        $("#sound").html("&#128277;");
        song.pause();
        window.clearInterval(interval);
        interval = false;
    } else {
        if(tillPlayed === undefined) {
            setCookie('playTime',0.0);
        }
        song.currentTime = tillPlayed;
        song.play();
        $("#sound").html("&#128276;");
        interval = setInterval(update,500);
    }
    played = getCookie('isPlaying');
}

function update() {
    if(played === "true"){
        setCookie('playTime', song.currentTime);
    }
}

$("#sound").click(function() {
    if(played === "true") {
        played = "false";
        setCookie('isPlaying', "false");
        setCookie('playTime',0.0);
        window.clearInterval(interval);
        interval = false;
        song.pause();
        song.currentTime = 0.0;
        $(this).html("&#128277;");
    } else {
        played = "true";
        setCookie('isPlaying', "true");
        setCookie('playTime',0.0);
        interval = setInterval(update,500);
        song.play();
        $(this).html("&#128276;");
    }
});
<?php
session_start();
include_once("utils/utilities.php");

if(!isset($_SESSION['login'])) {
    header("location: ./");
    exit();
}
if(isset($_SESSION['room1']) && isset($_SESSION['room2'])) {
    header("location: gamelobby.php");
    exit();
}
if(isset($_SESSION['gamenr'])) {
    header("location: game.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Mafia: Lobby</title>
    <link rel="icon" href="./img/j.png" type="image/png" sizes="16x16">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/lobby.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono" rel="stylesheet">
</head>
<body>
<header>
    <iframe src="audio/silence.mp3" id="musicframe"></iframe>
    <audio id="audio" autoplay controls loop><source src="audio/sound.mp3" type="audio/mp3"></audio>
</header>

<main>
    <h1>Hi, <?php echo $_SESSION['login']?>!</h1>
    <p>Let's feel like Vito Corleone...</p>
    <div class="link-wrapper" id="new-game-button">
        <a href="#" onclick="return false;">Create new game</a>
    </div>
    <div class="newgame-form">
        <form action="forms/creategame.php" method="post">
            <label>
                Game password:
                <input type="password" name="room-pass">
            </label>
            <label>
                # of players:
                <input type="number" min="3" max="8" value="3" name="room-players">
            </label>
            <input type="submit" value="Create!">
        </form>
    </div>
    <p class="warning"><?php utilities::show("alertNG"); ?></p>
    <div class="link-wrapper" id="existing-game-button">
        <a href="#" onclick="return false;">Join existing game</a>
    </div>
    <div class="existinggame-form">
        <form action="forms/joingame.php" method="post">
            <label>
                Game number:
                <input type="text" name="room-nr">
            </label>
            <label>
                Game password:
                <input type="password" name="room-pass">
            </label>
            <input type="submit" value="Join!">
        </form>
    </div>
    <p class="warning"><?php utilities::show("alertJEG"); ?></p>
    <div class="link-wrapper">
        <a href="settings.php">Profile settings</a>
    </div>
    <a href="forms/logout.php">Logout</a>
</main>
<aside>
    <div id="sound"></div>
    <img id="background" src="img/intro1.jpg" alt="background">
</aside>
<footer>
</footer>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/music.js"></script>
<script src="js/script.js"></script>
</body>
</html>
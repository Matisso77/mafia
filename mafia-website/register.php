<?php
include("utils/utilities.php");
session_start();
if(isset($_SESSION['login'])) {
    header("location: lobby.php");
    exit();
}

?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Mafia: Register</title>
    <link rel="icon" href="./img/j.png" type="image/png" sizes="16x16">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono" rel="stylesheet">
</head>
<body>
<header>
    <iframe src="audio/silence.mp3" id="musicframe"></iframe>
    <audio id="audio" autoplay controls loop><source src="audio/sound.mp3" type="audio/mp3"></audio>
</header>

<main>
    <h1>Mafia</h1>
    <form action="forms/register.php" method="post">
        <label>
            Login:
            <input type="text" name="login">
        </label>
        <label>
            Mail:
            <input type="text" name="email">
        </label>
        <label>
            Password:
            <input type="password" name="password1">
        </label>
        <label>
            Confirm password:
            <input type="password" name="password2">
        </label>
        <input type="submit" value="Sign up">
    </form>
    <p class="warning"><?php utilities::show("alertRegister"); ?></p>
    <a href="./">Back to sign in</a>
</main>
<aside>
    <div id="sound"></div>
    <img id="background" src="img/intro1.jpg" alt="background">
</aside>
<footer>
</footer>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/music.js"></script>
<script src="js/script.js"></script>
</body>
</html>
<?php

session_start();
include("../utils/utilities.php");

if(!isset($_SESSION['login'])) {
    header("location: ./");
    exit();
}
if(!isset($_SESSION['gamenr'])) {
    header("location: ./../lobby.php");
    exit();
}
$url = 'http://localhost:8080/api/lobby/gamePingVote';

$req = array(
    "roomNumber" => intval($_SESSION['gamenr']),
    "player" => $_SESSION['login'],
    "stage" => 'podciągCiąguJestCiągiemPodciągu'
);
$result = utilities::post($url, $req);
$result = json_decode($result, true);
$result=$result['list'];

$response['h1'] = "";
$response['p'] = "";
$response['voting'] = "";

if(isset($result['message'])) {
        $response['p'] = $result['message'];
} else if(isset($result['information'])) {
        $response['voting'] = 'already';
} else if(isset($result['WhoKilledDay']) && isset($result['DayNumber'])) {
    $response['h1'] = "Game - day " . $result['DayNumber'];
    $response['p'] = ($result['WhoKilledDay'] . "was just killed, the game is over for him..");
} else if(isset($result['dayNumber']) && isset($result['targetList']) && isset($result['policeman']) && isset($result['playerPoliceman'])) {
    $response['h1'] = "Game - day " . $result['dayNumber'];
    $response['p'] = ($result['playerPoliceman'] . ' who was checked previous night is ' . ($result['policeman'] ? "a" : "not a") . " mafia member!<br>");
    $response['voting'] = utilities::makeVoting("Now there is a voting..", $result['targetList']);
} else if(isset($result['dayNumber']) && isset($result['targetList'])) {
    $response['h1'] = "Game - day " . $result['dayNumber'];
    $response['voting'] = utilities::makeVoting("Now there is a voting..", $result['targetList']);
} else if(isset($result['listOfTargetsM']) && $result['listOfTargetsM'] != null && $result['listOfTargetsM'] != 'null' && isset($result['Night'])) {
    $response['h1'] = "Game - night " . $result['Night'];
    $response['voting'] = utilities::makeVoting("Who can endanger you, Mr Mafiozo?", $result['listOfTargetsM']);
} else if(isset($result['listOfTargetsP']) && $result['listOfTargetsP'] != null && $result['listOfTargetsP'] != 'null'  && isset($result['Night'])) {
    $response['h1'] = "Game - night " . $result['Night'];
    $response['voting'] = utilities::makeVoting("Who is worth checking Mr Policeman?", $result['listOfTargetsP']);
} else if(isset($result['Night'])) {
    $response['h1'] = "Game - night " . $result['Night'];
} else {
    echo "null";
    exit();
}

echo json_encode($response);
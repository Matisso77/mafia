<?php
include("../utils/utilities.php");
session_start();

if(empty($_POST['login']) || empty($_POST['email'])) {
    $_SESSION['alertReminder'] = "Type login and mail!";
    header("location: ../newpassword.php");
    exit();
}

$url1 = 'http://localhost:8080/api/login/forget';
$url2 = 'http://localhost:8080/api/login/forget';//todo change address
$req = array(
    "username" => $_POST['login'],
    "email" => $_POST['email']
);

$result = utilities::post($url1, $req);
$result = json_decode($result, true);

if(!isset($result["ErrorMessage"])) {
    $pass = utilities::generateSalt(12);
    $salt = utilities::generateSalt(50);
    $saltedpass = password_hash($pass, PASSWORD_DEFAULT, ["salt" => $salt]);

    $req = array(
        "username" => $_POST['login'],
        "pass" => $saltedpass,
        "salt" => $salt
    );

    $result = utilities::post($url2, $req);
    $result = json_decode($result, true);
    if(!true) //todo jakas wiadomosc od serwera
        utilities::sendNewPassword($_POST['login'], $_POST['email'], $pass);
}

$_SESSION['alertReminder'] = "If the combination of username and mail was correct, the new password has been sent!";
header("location: ../newpassword.php");
exit();
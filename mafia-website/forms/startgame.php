<?php
include("../utils/utilities.php");
session_start();

if(!isset($_SESSION['login'])) {
    header("location: ./");
    exit();
}
if(!(isset($_SESSION['room1']) && isset($_SESSION['room2'])&& isset($_SESSION['room3']))) {
    header("location: lobby.php");
    exit();
}

$url1 = 'http://localhost:8080/api/lobby/start';

$req1 = array(
    "roomNumber" => $_SESSION['room1']
);

$result = utilities::post($url1, $req1);

if(!$result) {
    $_SESSION['alertLogin'] = "Server error, try again later!";
    header("location: ../");
    exit();
}
$result = json_decode($result, true);
if($result['Message'] != "Start requested!")
    $_SESSION['errorik'] = $result['Message'];

header('location: ../gamelobby.php');
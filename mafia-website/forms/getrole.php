<?php

session_start();
include("../utils/utilities.php");

if(!isset($_SESSION['login'])) {
    header("location: ./");
    exit();
}
if(!isset($_SESSION['gamenr'])) {
    header("location: ./../lobby.php");
    exit();
}
$url = 'http://localhost:8080/api/lobby/sendRoles';

$req = array(
    "roomNumber" => intval($_SESSION['gamenr']),
    "player" => $_SESSION['login']
);
$result = utilities::post($url, $req);
$result = json_decode($result, true);

echo (($result['role'] == null) ? "null" : $result['role']);
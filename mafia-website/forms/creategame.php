<?php
session_start();
include("../utils/utilities.php");

if(!isset($_SESSION['login'])) {
    header("location: ./");
    exit();
}
if(!isset($_POST['room-pass']) || !isset($_POST['room-players'])) {
    header("location: ../lobby.php");
    exit();
}
if(strlen($_POST['room-pass']) < 5) {
    header("location: ../lobby.php");
    $_SESSION['alertNG'] = 'Too short password (at least 5 signs)!';
    exit();
}
$url = 'http://localhost:8080/api/lobby/create';
$size = intval($_POST['room-players']);
$req = array(
    "username" => $_SESSION['login'],
    "roomPassword" => $_POST['room-pass'],
    "roomSize" => $size
);
$result = utilities::post($url, $req);
$result = json_decode($result, true);

if(!isset($result["roomNumber"])) {
    $_SESSION['alertNG'] = 'Error while creating the room!';
    header("location: ../lobby.php");
    exit();
}

$_SESSION['room1'] = $result["roomNumber"];
$_SESSION['room2'] = $_POST['room-pass'];
$_SESSION['room3'] = $_POST['room-players'];
header("location: ../gamelobby.php");
<?php
include("../utils/utilities.php");
session_start();

$flag = 0;

if(!isset($_SESSION['room1']) || !isset($_SESSION['room2']) || !isset($_SESSION['room3'])) {
    $flag++;
}
if(!isset($_SESSION['roomnr'])) {
    $flag++;
}
if($flag != 1) {
    header("location: ../lobby.php");
    exit();
}

if(!isset($_SESSION['roomnr'])) {
    $url = 'http://localhost:8080/api/lobby/leave';
    $req = array(
        "userName" => $_SESSION['login'],
        "roomNumber" => $_SESSION['room1']
    );

    unset($_SESSION['room1']);
    unset($_SESSION['room2']);
    unset($_SESSION['room3']);

    $result = utilities::post($url, $req);
    var_dump($result);
    $result = json_decode($result, true);
}
else {
    //ktos wyszedl z dzialajacej gry todo
    unset($_SESSION['roomnr']);
}

header("location: ../abandoned.php");
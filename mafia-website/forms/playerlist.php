<?php
session_start();
include("../utils/utilities.php");

if(!isset($_SESSION['login'])) {
    header("location: ./");
    exit();
}
if(!(isset($_SESSION['room1']) && isset($_SESSION['room2']))) {
    header("location: ../lobby.php");
    exit();
}
$url = 'http://localhost:8080/api/lobby/players';

$req = array(
    "roomNumber" => intval($_SESSION['room1'])
);
$result = utilities::post($url, $req);
$result = json_decode($result, true);

if(!$result || (!isset($result['list']) || !isset($result['status']))) {
    //todo server err
}

echo $result['status'].':'.count($result['list']);
<?php
session_start();
include("../utils/utilities.php");

if(!isset($_SESSION['login'])) {
    header("location: ./");
    exit();
}
if(!isset($_POST['room-pass']) || !isset($_POST['room-nr'])) {
    header("location: ../lobby.php");
    exit();
}
$url = 'http://localhost:8080/api/lobby/join';

$nr = intval($_POST['room-nr']);
$req = array(
    "userName" => $_SESSION['login'],
    "roomPassword" => $_POST['room-pass'],
    "roomNumber" => $nr
);
$result = utilities::post($url, $req);
$result = json_decode($result, true);

if(!isset($result["roomNumber"])) {
    $_SESSION['alertJEG'] = $result['Message'];
    header("location: ../lobby.php");
    exit();
}

$_SESSION['room1'] = $nr;
$_SESSION['room2'] = $_POST['room-pass'];
$_SESSION['room3'] = $result['roomSize'];
header("location: ../gamelobby.php");
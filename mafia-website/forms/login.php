<?php
include("../utils/utilities.php");
session_start();

if(!(!empty($_POST['login'])) && !empty($_POST['password'])) {
    $_SESSION['alertLogin'] = "Type login and password!";
    header("location: ../");
    exit();
}

$url1 = 'http://localhost:8080/api/login/salt';
$url2 = 'http://localhost:8080/api/login/password';
$req1 = array(
    "username" => $_POST['login']
);

$result = utilities::post($url1, $req1);
if(!$result) {
    $_SESSION['alertLogin'] = "Server error, try again later!";
    header("location: ../");
    exit();
}
if(!(isset($result['salt'])) && isset($result['username'])) {
    $_SESSION['alertLogin'] = "Uncorrect login or password!";
    header("location: ../");
    exit();
}
$result = json_decode($result, true);

$saltedpass = password_hash($_POST['password'], PASSWORD_DEFAULT, ["salt" => $result["salt"]]);

$req2 = array(
    "username" => $_POST['login'],
    "password" => $saltedpass
);

$result = utilities::post($url2, $req2);
if(!$result) {
    $_SESSION['alertLogin'] = "Server error, try again later!";
    header("location: ../");
    exit();
}
$result = json_decode($result, true);

if(!(isset($result['username']))) {
    $_SESSION['alertLogin'] = "Uncorrect login or password!";
    header("location: ../");
    exit();
} else {
    $_SESSION['login'] = $result['username'];
    header('location: ../lobby.php');
    exit();
}
<?php
include("../utils/utilities.php");
session_start();

if(!isset($_SESSION['login'])) {
    header("location: ./");
    exit();
}

if(!(!empty($_POST['old-pass'])) && !empty($_POST['new-pass'])) {
    $_SESSION['alertLogin'] = "Type both passwords!";
    header("location: ../settings.php");
    exit();
}


if($_POST['old-pass'] == $_POST['new-pass']) {
    $_SESSION['alertLogin'] = "Old and new password are the same!";
    header("location: ../settings.php");
    exit();
}
if(strlen($_POST['new-pass']) < 8) {
    $_SESSION['alertLogin'] = "New pass should have at least 8 signs!";
    header("location: ../settings.php");
    exit();
}

$url1 = 'http://localhost:8080/api/login/salt';
$url2 = 'http://localhost:8080/api/login/password';
$req1 = array(
    "username" => $_POST['login']
);

$result = utilities::post($url1, $req1);
if(!$result) {
    $_SESSION['alertLogin'] = "Server error, try again later!";
    header("location: ../");
    exit();
}
if(!(isset($result['salt'])) && isset($result['username'])) {
    $_SESSION['alertLogin'] = "Uncorrect login or password!";
    header("location: ../");
    exit();
}
$result = json_decode($result, true);

$saltedoldpass = password_hash($_POST['old-pass'], PASSWORD_DEFAULT, ["salt" => $result["salt"]]);
$salt = utilities::generateSalt(50);
$saltedpass = password_hash($_POST['new-pass'], PASSWORD_DEFAULT, ["salt" => $salt]);

$req = array(
    "username" => $_POST['login'],
    "old-pass" => $saltedoldpass,
    "new-pass" => $saltedpass,
    "salt" => $salt
);

$result = utilities::post($url2, $req);
if(!$result) {
    $_SESSION['alertLogin'] = "Server error, try again later!";
    header("location: ../settings.php");
    exit();
}

$result = json_decode($result, true);

//if(isset()) {
if(true) {
    $_SESSION['alertLogin'] = "Password has been changed!";
    header("location: ../settings.php");
    exit();
}
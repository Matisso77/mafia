<?php
include("../utils/utilities.php");
session_start();

if(empty($_POST['login']) || empty($_POST['email']) || empty($_POST['password1']) || empty($_POST['password2'])) {
    $_SESSION['alertRegister'] = "Type login, mail and password!";
    header("location: ../register.php");
    exit();
}

if($_POST['login'] != utilities::checkUser($_POST['login'])) {
    $_SESSION['alertRegister'] = "Username must have between 4 and 16 signs and contains only [A-Za-z_-] signs!";
    header("location: ../register.php");
    exit();
}
if($_POST['password1'] != utilities::checkPass($_POST['password1'], $_POST['password2'])) {
    $_SESSION['alertRegister'] = "Passwords has to be the same and have at least 8 signs!";
    header("location: ../register.php");
    exit();
}
if($_POST['email'] != utilities::checkMail($_POST['email'])) {
    $_SESSION['alertRegister'] = "Entered email was invalid!";
    header("location: ../register.php");
    exit();
}

$url = 'http://localhost:8080/api/login/register';
$salt = utilities::generateSalt(50);
$req = array(
    "username" => $_POST['login'],
    "email" => $_POST['email'],
    "salt" => $salt,
    "password" => password_hash($_POST['password1'], PASSWORD_DEFAULT, [ "salt" => $salt ])
);

$result = utilities::post($url, $req);
$result = json_decode($result, true);

if(isset($result['Error'])) {
    $_SESSION['alertRegister'] = $result['Error'];
    header("location: ../register.php");
    exit();
} else {
    $_SESSION['alertRegister1'] = "You have been registered! Let's log in!";
    header("location: ../");
    exit();
}
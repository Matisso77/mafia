<?php

session_start();
include("../utils/utilities.php");

if(!isset($_SESSION['login'])) {
    header("location: ./");
    exit();
}
if(!isset($_SESSION['gamenr'])) {
    header("location: ./../lobby.php");
    exit();
}
$url = 'http://localhost:8080/api/lobby/gamePing';

$req = array(
    "roomNumber" => intval($_SESSION['gamenr']),
    "player" => $_SESSION['login']
);
$result = utilities::post($url, $req);
$result = json_decode($result, true);
$result = $result['list'];

$list['code'] = '';
$list['dead'] = '';
$playerlist = array();
foreach ($result as $key => $value) {
    if($value == "ALIVE") {
        $list['code'] = $list['code'].'<p>'.$key.'</p>';
        $playerlist[] = $key;
    } else if($value == "DEAD") {
        $list['code'] = $list['code'].'<p class="eliminated">'.$key.'</p>';
    } else if ($value == "MYSELF") {
        $list['code'] = $list['code'] . '<p class="myself">' . $key . '</p>';
        $playerlist[] = $key;
    }
}
if(!isset($_SESSION['playerlist'])) {
    $_SESSION['playerlist'] = $playerlist;
}

//sesyjna - stara lista zywych, zmienna - nowa lista zywych
//szukam po starej liscie takiego zawodnika ktorego nie ma w nowej

for($i=0; $i<count($_SESSION['playerlist']); ++$i) {
    $player = $_SESSION['playerlist'][$i];
    $found = false;
    for($j=0; $j<count($playerlist); ++$j) {
        if($playerlist[$j] == $player) {
            $found = true;
            break;
        }
    }
    if(!$found){
        $list['dead'] = 'Last death: ' . $player;
        if($player == $_SESSION['login']) {
            $list['me'] = 'true';
        }
        break;
    }
}

$_SESSION['playerlist'] = $playerlist;

echo json_encode($list);
<?php
session_start();
include("../utils/utilities.php");
if(!isset($_SESSION['login'])) {
    header("location: ./../");
    exit();
}
if(!isset($_SESSION['gamenr'])) {
    header("location: ./../lobby.php");
    exit();
}
if(!isset($_POST['target'])) {
    echo "ERROR";
    exit();
}

$url = 'http://localhost:8080/api/lobby/sendData';

$req = array(
    "roomNumber" => intval($_SESSION['gamenr']),
    "vote" => 'K',
    "whoVote" => $_SESSION['login'],
    "whoGetVote" => $_POST['target']
);
$result = utilities::post($url, $req);
$result = json_decode($result, true);

echo $result['Message'];
<?php
include("utils/utilities.php");
session_start();

if(isset($_SESSION['login'])) {
    header("location: lobby.php");
    exit();
}
if(isset($_SESSION['gamenr'])) {
    header("location: game.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Mafia: Main site</title>
    <link rel="icon" href="./img/j.png" type="image/png" sizes="16x16">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono" rel="stylesheet">
</head>
<body>
<header>
    <iframe src="audio/silence.mp3" id="musicframe"></iframe>
    <audio id="audio" autoplay controls loop><source src="audio/sound.mp3" type="audio/mp3"></audio>
</header>

<main>
<h1>Mafia</h1>
    <p>Sign in to play the game!</p>
    <form action="forms/login.php" method="post">
        <label>
            Login:
            <input type="text" name="login">
        </label>
        <label>
            Password:
            <input type="password" name="password">
        </label>
        <input type="submit" value="Sign in">
    </form>
    <p class="warning"><?php utilities::show("alertLogin"); ?></p>
    <p style="color: green;"><?php utilities::show("alertRegister1"); ?></p>
    <a href="register.php">Sign up</a>
    <a href="newpassword.php">Forgotten password?</a>
</main>
<aside>
    <div id="sound"></div>
    <img id="background" src="img/intro1.jpg" alt="background">
</aside>
<footer>
</footer>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/music.js"></script>
<script src="js/script.js"></script>
</body>
</html>